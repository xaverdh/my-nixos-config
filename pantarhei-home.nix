{ pkgs, config, lib, ... }:
let
  inherit (pkgs) my-lib;
  bind = my-lib.mount.bind;

  home = config.custom.pantarhei.home;
  confd = "${home}/config";

  mounts = my-lib.bindAttrs (p: v: {
      "${home}/${p}" = bind { src = v; owner = "pantarhei"; };
  })
  {
    ".background" = "${confd}/background";
    ".config/htop" = "${confd}/htop";
    ".config/git" = "${confd}/git";
    ".links" = "${confd}/links";
    ".config" = "/persist/home/pantarhei/.config";
    ".local" = "/persist/home/pantarhei/.local";
    ".cache" = "/persist/home/pantarhei/.cache";
    ".rustup" = "/persist/home/pantarhei/.rustup";
    ".cargo" = "/persist/home/pantarhei/.cargo";
    ".android" = "/persist/home/pantarhei/.android";
    ".ssh" = "/persist/home/pantarhei/.ssh";
    ".offlineimap" = "/persist/home/pantarhei/.offlineimap";
  };
in
{
  fileSystems = mounts // {
    "${home}/.gnupg" = bind {
      src = "/persist/home/pantarhei/.gnupg";
      owner = "pantarhei";
      mode = "0700";
    };
  };
}

