# User pantarhei's packages
{ config, pkgs, lib, ... }:
let
  inherit (pkgs) wrappers;
  home = config.custom.pantarhei.home;

  my-firefox = pkgs.callPackage ./firefox/my-firefox.nix {
    inherit config;
    inherit (pkgs) my-lib;
  };

  firefox = my-firefox.override {
    darkTheme = true;
    passOptions.profileLocation = "${home}/.local/share/firefox";
  };

  firefox-scripting = my-firefox.override {
    withScripting = true;
    passOptions = {
      wmClass = "Firefox-Scripting";
      exeName = "firefox-scripting";
      profileLocation = "${home}/.local/share/firefox-scripting";
    };
  };

  mpv-mpris = pkgs.mpv.override {
    scripts = [ pkgs.mpvScripts.mpris ];
  };
in
{
  custom.pantarhei.pkgs = with pkgs; [
    passwords
    tokei
    firefox
    firefox-scripting
    mpv-mpris
    yt-dlp
    scrcpy
    graphicsmagick
    system-info
    wrappers.bluetoothctl
    wrappers.tig
    wrappers.neomutt-runbox
    wrappers.neomutt-gmx
    wrappers.offlineimap
    wrappers.abook
    aspell
    aspellDicts.en
    aspellDicts.de
    calendar-cli
    sdat2img
    mqttui
    AusweisApp2
  ]
  ++ my-scripts
  ++ lib.optionals (!config.custom.minimal) [
    marble mumble tor-browser-bundle-bin
    haskell-language-server
    whisper-ctranslate2
  ];

  systemd.user.services.mpris-proxy = {
    description = "Forward bluetooth midi controls via mpris2 so they are picked up by supporting media players";
    serviceConfig.ExecStart = "${lib.getBin pkgs.bluez}/bin/mpris-proxy";
    wantedBy = [ "default.target" ];
  };
}
