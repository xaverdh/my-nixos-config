{ boxName, pkgs, nix }:
let
  stage1 = pkgs.writers.writeBashBin "rebuild-nixos" ''
    if [[ -z "$PATH_TO_CONFIG" ]]; then
      PATH_TO_CONFIG="$HOME/git/my-nixos-config"
    fi
    if [[ -z "$BOX_NAME" ]]; then
      BOX_NAME="${boxName}"
    fi
    if [[ -z "$PATH_TO_PROFILE" ]]; then
      PATH_TO_PROFILE=/nix/var/nix/profiles/system
    fi

    case "$1" in
      switch|boot|test|dry-activate)
        ACTION="$1"; shift
      ;;
      *)
      echo "unknown action: $1"
      exit 1
      ;;
    esac

    export BUILD_SPEC="$PATH_TO_CONFIG#nixosConfigurations.$BOX_NAME.config.system.build.toplevel"
    export PATH_TO_PROFILE
    export ACTION

    ${nix}/bin/nix --verbose shell "$BUILD_SPEC" "$@" -c ${stage2}/bin/rebuild-nixos-stage2
  '';
  stage2 = pkgs.writers.writeBashBin "rebuild-nixos-stage2" ''
    export SWITCH_TO_CONFIGURATION=$(realpath $(type -P switch-to-configuration))
    export RESULT_PATH=$(dirname $(dirname "$SWITCH_TO_CONFIGURATION"))
    sudo --preserve-env=PATH_TO_PROFILE,ACTION,RESULT_PATH,SWITCH_TO_CONFIGURATION \
      ${stage3}/bin/rebuild-nixos-stage3
  '';
  stage3 = pkgs.writers.writeBashBin "rebuild-nixos-stage3" ''
    ${nix}/bin/nix-env -p "$PATH_TO_PROFILE" --set "$RESULT_PATH"
    "$SWITCH_TO_CONFIGURATION" "$ACTION"
  '';
in stage1
