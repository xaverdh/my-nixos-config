{ writers, lib, gnused }:
writers.writeBashBin "unnix" ''
  NIX_STORE=''${NIX_STORE:-/nix/store}
  ${lib.getExe gnused} s@"$NIX_STORE"'/[a-z0-9]\{32\}-@/<<NIX>>/@g'
''
