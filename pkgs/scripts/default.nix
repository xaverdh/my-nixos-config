{ pkgs, haskellPackages ? pkgs.haskellPackages, ... }:
let
  mk = name: args:
    pkgs.writers.writeHaskellBin name args (builtins.readFile "${src}/${name}.hs");
  src = pkgs.fetchgit {
    url = "https://gitlab.com/xaverdh/scripts";
    rev = "7a26b810b5f1d38b3f5a8847cce68dabad95c577";
    hash = "sha256-7yT1eqJrrkQYM7ZBEFRtMLeqlpXN9+N/EKq5ploz7n4=";
  };
in
  with haskellPackages; [
    (mk "ghc-with" { inherit ghc; libraries = [ optparse-applicative ]; })
    (mk "ffxrecord" { inherit ghc; libraries = [ optparse-applicative X11 ]; })
    (mk "nixpkgs-lookup" { inherit ghc; libraries = [ http-conduit optparse-applicative data-default ]; })
    (mk "qr2wifi" { inherit ghc; libraries = [ process parsers bytestring attoparsec optparse-applicative data-default ]; })
    (mk "wifi2qr" { inherit ghc; libraries = [ process optparse-applicative bytestring hex ini ]; })
    (mk "eduroam-setup" { inherit ghc; libraries = [ optparse-applicative haskeline ]; })
    (mk "watch-bluez-player-disconnect" { inherit ghc; libraries = [ extra dbus containers ]; })
  ]
