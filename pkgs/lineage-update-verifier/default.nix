{ stdenv, fetchFromGitHub, lib, makeWrapper, python3, openssl }:
stdenv.mkDerivation rec {
  pname = "lineage_update_verifier";
  version = "unstable-2019-04-08";
  src = fetchFromGitHub {
    repo = "update_verifier";
    owner = "LineageOS";
    rev = "0c65c4f13c489e18b9cf6be9c11f54794217ae5a";
    sha256 = "0pdnvgfhrxrrlg6xbi29620fpbzg2k183dqp15yszzslfljb5xhg";
  };
  nativeBuildInputs = [ makeWrapper ];
  python = python3.withPackages (ps: with ps; [ asn1crypto oscrypto ]);
  # TODO: make this less hacky
  buildCommand = ''
    mkdir -p $out/bin
    echo '#!/usr/bin/env nix-shell' > $out/bin/update_verifier
    echo '#!nix-shell -I nixpkgs=flake:nixpkgs -i python -p openssl' >> $out/bin/update_verifier
    cat < $src/update_verifier.py >> $out/bin/update_verifier
    chmod a+x $out/bin/update_verifier
    wrapProgram $out/bin/update_verifier --prefix PATH : ${lib.makeBinPath [ python openssl ]}
  '';
  meta = with lib; {
    description = "Verify signatures on LineageOS update files";
    homepage = "https://github.com/LineageOS/update_verifier";
    license = licenses.apsl20;
    maintainers = [ maintainers.xaverdh ];
    platforms = platforms.unix;
  };
}
