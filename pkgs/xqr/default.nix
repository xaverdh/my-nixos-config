{ pkgs, lib, xclip, qrencode, feh }:
pkgs.writers.writeBashBin "xqr" ''
  ${lib.getExe xclip} -selection clipboard -o \
    | ${lib.getExe qrencode} -o - -- "$@" \
    | ${lib.getExe feh} -
''
