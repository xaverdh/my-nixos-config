{ my-lib, pkgs }:
(my-lib.haskell pkgs.haskellPackages).mkPackage {
    name = "system-info";
    src = my-lib.fetchGitL {
      url = "https://gitlab.com/xaverdh/system-info.git/";
      rev = "a021b91e98188d09a3b43d58dbb64178c205b602";
      hash = "sha256-vrS+2vS2ZIaqtA+MZVSXjYLUDqiqyZCPA2FNPqGgAhQ=";
    };
  } {}
