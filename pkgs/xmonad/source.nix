{ my-lib }:
my-lib.fetchGitL {
  url = "https://gitlab.com/xaverdh/my-xmonad-config";
  rev = "bb156200c83f39a4d98c2a6ec8245dcbad57c2fa";
  hash = "sha256-piRW0HLX4KqRN6DJzFg4R8WNzuDfUXq2EF40v77Uh5Q=";
  #lpath = /home/pantarhei/git/my-xmonad-config;
}
