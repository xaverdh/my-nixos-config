{ my-lib, pkgs, my-xmonad-src, haskellPackages }:
let
  haskellFuncs = my-lib.haskell haskellPackages;
in haskellFuncs.mkPackage {
  name = "my-xmonad-lib";
  src = my-xmonad-src;
  cabal2nixOptions = "--subpath lib ";
} { }
