{ lib, writers, coreutils, pinentry-curses, libargon2, gnused }:
let
  pinentry = lib.getExe pinentry-curses;
  argon2 = lib.getExe libargon2;
  sed = lib.getExe gnused;
  tty = lib.getExe' coreutils "tty";
in writers.writeBashBin "passwords" ''
  tty=$(${tty})
  echo "GETPIN" | ${pinentry} --timeout 0 -T $tty | ${sed} -nr '0,/^D (.+)/s//\1/p' \
  | ${argon2} "$@" -id -t 1 -m 21 -p 4
  # https://datatracker.ietf.org/doc/html/rfc9106#name-parameter-choice
''
