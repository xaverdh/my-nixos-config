{ stdenv, lib, fetchFromGitHub, python3, makeWrapper }:
stdenv.mkDerivation {
  pname = "sdat2img";
  version = "unstable-2018-10-30";
  src = fetchFromGitHub {
    repo = "sdat2img";
    owner = "xpirt";
    rev = "1b08432247fce8037fd6a43685c6e7037a2e553a";
    sha256 = "12s9jc7mr1dyb2069zl8cb9hs4bf9gc283csqlrbxnq6gaxcrg5l";
  };
  nativeBuildInputs = [ makeWrapper ];
  buildCommand = ''
    install -D $src/sdat2img.py $out/bin/sdat2img
    wrapProgram $out/bin/sdat2img --prefix PATH : ${lib.makeBinPath [ python3 ]}
  '';
  meta = {
    description = "Convert sparse Android data image (.dat) into filesystem ext4 image (.img)";
    homepage = "https://github.com/xpirt/sdat2img";
    license = {
      fullName = "xpirt custom license";
      url = "https://forum.xda-developers.com/android/software-hacking/how-to-conver-lollipop-dat-files-to-t2978952";
    };
    maintainers = [ lib.maintainers.xaverdh ];
    platforms = lib.platforms.unix;
  };
}
