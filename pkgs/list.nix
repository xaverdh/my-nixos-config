{ config }: self: super:

super.lib.fix (this: with this;
  let callPackage = self.newScope this; in {

  my-lib = callPackage ../lib.nix {};

  wrappers = callPackage ../wrappers.nix {
    secrets = config.custom.pantarhei.secrets;
  };

  my-xmonad-src = callPackage ./xmonad/source.nix {};

  my-xmonad-lib = hpkgs: callPackage ./xmonad/lib.nix {
    haskellPackages = hpkgs;
  };

  system-info = callPackage ./system-info {
  };

  lineage-update-verifier = callPackage ./lineage-update-verifier {};

  my-scripts = callPackage ./scripts {};

  sdat2img = callPackage ./sdat2img {};

  xqr = callPackage ./xqr {};

  rebuild-nixos = callPackage ./rebuild-nixos { inherit (config.custom) boxName; };

  tuxedo-silence-fan = callPackage ../per-box/pegasus/tuxedo-silence-fan {};

  unnix = callPackage ./unnix {};

  passwords = callPackage ./passwords {};

})
