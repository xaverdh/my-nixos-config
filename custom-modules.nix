{
  imports = [
    ./modules/global.nix
    ./modules/nix-serve.nix
    ./modules/xserver.nix
    ./modules/terminal.nix
    ./modules/dmenu.nix
    ./modules/dunst.nix
    ./modules/linux.nix
    ./modules/systemd.nix
    ./modules/mounts.nix
    ./modules/alarm-clock.nix
    ./modules/networking.nix
  ];
}
