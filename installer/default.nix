{ pkgs ? import <nixpkgs> {}
, lib ? pkgs.lib
, systemName
, flake
, extraDataSubvols ? [ "data" "config" "git" "secrets" "mail" ]
, mountPrefix ? "/tmp/install-${systemName}"
}:
let
  mkpasswd = "${lib.getExe pkgs.mkpasswd}";
  chmod = "${lib.getBin pkgs.coreutils}/bin/chmod";
  cut = "${lib.getBin pkgs.coreutils}/bin/cut";
  writePasswordFile = { user, path }: ''
    while true; do
      echo "enter password for user ${user}"
      ${mkpasswd} -m yescrypt > "${path}"
      echo "enter password for user ${user} again"
      if diff "${path}" <(${mkpasswd} -m yescrypt -S $(${cut} -d'$' -f-4 < "${path}"))
      then
        break
      else
        echo "passwords do not match, try again"
      fi
    done
    ${chmod} 0600 ${path}
  '';
in pkgs.callPackage ./installer.nix {
  inherit pkgs systemName mountPrefix;
  systemFlakePath = "${flake}";
  partitions = [
     {
      conf = {
        Label = "${systemName}-esp";
        Type = "esp";
        SizeMinBytes = "1024M";
        SizeMaxBytes = "1024M";
        Format = "vfat";
      };
      mounts."/boot" = {};
    }

    {
      conf = {
        Label = "${systemName}-persist";
        Type = "linux-generic";
        Weight = "1000";
        Format = "ext4";
      };
      contents = flake.fileSystemSpec.persist "";
      extraSetup = mountpoint: ''
        nix-store --generate-binary-cache-key ${systemName}-store-key \
        ${mountpoint}/var/lib/nix-store-key/cache-secret \
        ${mountpoint}/var/lib/nix-store-key/cache-public
        chmod 0400 ${mountpoint}/var/lib/nix-store-key/cache-secret
        chmod 0444 ${mountpoint}/var/lib/nix-store-key/cache-public

        ${writePasswordFile {
            user = "root";
            path = "${mountpoint}/shadow/root";
          }}
        ${writePasswordFile {
            user = "pantarhei";
            path = "${mountpoint}/shadow/pantarhei";
          }}
      '';
      mounts."/persist" = {};
    }

    {
      conf = {
        Label = "${systemName}-data";
        Type = "linux-generic";
        Weight = "2000";
        Format = "btrfs";
      };
      contents = flake.fileSystemSpec.data "" ++ (
        let f = subvol: "v /${subvol} 0755 1000 100";
        in builtins.map f extraDataSubvols
      );
      mounts = {
        "/nix/store".options = [ "-tbtrfs" "-osubvol=store" ];
        "/nix/var".options = [ "-tbtrfs" "-osubvol=nix-var" ];
      };
    }
  ];
}
