{ pkgs
, lib ? pkgs.lib
, systemName
, partitions
, mountPrefix
, systemFlakePath
, setupRootPassword ? false
, ... }:
let
  jq = "${lib.getExe pkgs.jq}";
  unshare = "${lib.getBin pkgs.utillinux}/bin/unshare";
  mktemp = "${lib.getBin pkgs.coreutils}/bin/mktemp";
  nixos-install = "${lib.getBin pkgs.nixos-install-tools}/bin/nixos-install";

  term-colors = {
    reset = "$(tput sgr0)";
    white = "$(tput setaf 7)";
    green = "$(tput setaf 2)";
    blue = "$(tput setaf 4)";
    red = "$(tput setaf 1)";
    bold = "$(tput bold)";
  };

  log = s: with term-colors; "printf '%s\\n' ${bold}'[Install ${systemName}] '${green}${s}${reset}";
  logq = s: log ''"${s}"'';
  logqq = s: log "'${s}'";

  getPartLabel = p: p.conf.Label;
  generatePartitionFile = p: pkgs.writeTextDir "${getPartLabel p}.conf"
    ( lib.generators.toINI {} { Partition = p.conf; } );

  partSpec = pkgs.symlinkJoin {
    name = "partitions";
    paths = lib.map generatePartitionFile partitions;
  };

  setupMount = { partition, options ? [], mountpoint }:
    let partlabel = getPartLabel partition;
    in ''
    mkdir -p ${mountpoint}
    partuuid=$(${jq} -r 'map(select(.partlabel=="${partlabel}"))[0].partuuid' < "''${TMP_DIR}/partitions.json")
    mount ${lib.concatStringsSep " " options} "/dev/disk/by-partuuid/$partuuid" "${mountpoint}"
  '';


  setup-partitions = pkgs.writeShellScriptBin "setup-partitions" ''
    set -o pipefail -o errexit -o nounset
    ${logq "phase: setup-partitions"}

    ${lib.getBin pkgs.systemd}/bin/systemd-repart \
      --dry-run=no --definitions=${partSpec} --json=short -- "$@" \
      | ${jq} 'map({ partlabel : .label, partuuid : .uuid})' \
      > "''${TMP_DIR}/partitions.json"

    ${logq "waiting for kernel to read partitions"}
    ${lib.getBin pkgs.systemd}/bin/udevadm wait \
      $( ${jq} --raw-output 'map(.partuuid | "/dev/disk/by-partuuid/" +.) | .[]' \
         < "''${TMP_DIR}/partitions.json" )
  '';

  setup-skeleton = p:
    let
      partlabel = getPartLabel p;
      contents = p.contents or [];
      doExtraSetup = lib.optionalString (p?extraSetup) (
        p.extraSetup mountpoint
      );
      contentTmpfiles = pkgs.writeText "${partlabel}-tmpfiles" (
        lib.concatStringsSep "\n" contents
      );
      mountpoint = "${mountPrefix}/${partlabel}";
    in pkgs.writeShellScriptBin "setup-skeleton-${partlabel}" ''
      set -o pipefail -o errexit -o nounset
      ${logq "phase: setup-skeleton-${partlabel}"}

      mkdir -p "${mountPrefix}"
      mount -t tmpfs intaller-tmpfs "${mountPrefix}"

      ${setupMount { partition = p; inherit mountpoint; } }
      ${lib.getBin pkgs.systemd}/bin/systemd-tmpfiles \
        --create --root="${mountpoint}" ${contentTmpfiles}

      ${doExtraSetup}

      umount "${mountpoint}"
      umount "${mountPrefix}"
    '';

  setup-system =
    let
      mountsForPartition = p: lib.optionals (p?mounts)
        ( lib.mapAttrsToList
          ( where: config: setupMount {
              partition = p;
              mountpoint = "${mountPrefix}/${where}";
              options = config.options or [];
            } )
          p.mounts );
      setupAllMounts = lib.concatStringsSep "\n"
        ( lib.lists.flatten ( lib.map mountsForPartition partitions ) );
    in pkgs.writeShellScriptBin "setup-${systemName}" ''
      set -o pipefail -o errexit -o nounset
      ${logq "phase: setup-system"}

      mkdir -p "${mountPrefix}"
      mount -t tmpfs intaller-tmpfs "${mountPrefix}"

      ${setupAllMounts}

      cp -t "$TMP_DIR" -R "${systemFlakePath}"
      FLAKE_SPEC="$TMP_DIR/$(basename "${systemFlakePath}")#${systemName}"
      ${nixos-install} \
        ${lib.optionalString (!setupRootPassword) "--no-root-passwd"} \
        --root ${mountPrefix} \
        --flake "$FLAKE_SPEC" --verbose --impure \
        --option experimental-features "nix-command flakes auto-allocate-uids cgroups"
    '';

  install-system =
    let yes_no = with term-colors; "${bold}${white}[${green}y${white}/${red}n${white}]:${reset}";
        boldRed = with term-colors; "${bold}${red}";
    in pkgs.writeShellScriptBin "install-${systemName}" ''
      set -o pipefail -o errexit -o nounset

      test 0 -eq "$(id -u)" || { printf '%s\n' ${boldRed}"run me as root" ; exit 7; }

      test -e "$1" || { printf '%s\n' ${boldRed}"$1 does not exist" ; exit 9; }
      test -b "$1" || { printf '%s\n' ${boldRed}"$1 is not a block device"; exit 8; }

      ${logq "installing on $1"}

      while true
      do
        read -p "confirm ${yes_no} "
        case $REPLY in
          y*) break ;;
          n*) exit 0 ;;
        esac
      done

      TMP_DIR=''$(${mktemp} -d "''${TMPDIR:-/tmp/}$(basename $0).XXXXXXXXXX")
      echo "temporary directory is $TMP_DIR"
      export TMP_DIR
      ${lib.getExe setup-partitions} "$1"

      for exe in ${lib.concatStringsSep " " (lib.map (p: lib.getExe (setup-skeleton p)) partitions)}; do
        ${unshare} -m -- "$exe"
      done

      ${unshare} -m -- ${lib.getExe setup-system}
    '';
in {

  inherit install-system;

  env = pkgs.buildEnv {
    name = "${systemName}-installer";
    paths = [
      install-system # full installer
      setup-partitions
      setup-system
    ];
  };

}
