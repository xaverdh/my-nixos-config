{
  description = "installer for my systems";

  inputs.my-nixos-config.url = gitlab:xaverdh/my-nixos-config;

  outputs = { self, my-nixos-config }:
    let
      nixpkgs = my-nixos-config.inputs.nixpkgs; # use the same nixpkgs
      callInstaller = name: import ./default.nix {
        systemName = name;
        flake = my-nixos-config;
        pkgs = nixpkgs.legacyPackages.x86_64-linux;
      };
    in {
      packages.x86_64-linux = {
        tux = callInstaller "tux";
        seekers = callInstaller "seekers";
        generic = callInstaller "generic";
        pegasus = callInstaller "pegasus";
      };
    };
}
