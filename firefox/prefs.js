// See https://support.mozilla.org/en-US/kb/customizing-firefox-using-autoconfig for autoconfig docs
// Generic prefs docs:
//  https://wiki.mozilla.org/Privacy/Privacy_Task_Force/firefox_about_config_privacy_tweeks
//  http://kb.mozillazine.org/Firefox_%3a_FAQs_%3a_About%3aconfig_Entries
//  https://firefox-source-docs.mozilla.org/

// Security and Privacy related
//

// From the Tor Uplift project
// See https://wiki.mozilla.org/Security/Tor_Uplift
pref("privacy.resistFingerprinting",true);

// Enable full isolation between "frist parties"
pref("privacy.firstparty.isolate",true);

// Run iframes in seperate process
lockPref("fission.autostart",true);

// Show full urls in the adress bar
pref("browser.urlbar.trimURLs",false);

// Disable battery API
pref("dom.battery.enabled",false);

// Disable microphone/camera status access
pref("media.navigator.enabled",false);

// Don't notify websites on copy/cut/past
// or tell them what has been selected. 
pref("dom.event.clipboardevents.enabled",false);

// Prevent webpages from intercepting the page close event
// See https://developer.mozilla.org/en-US/docs/Web/API/WindowEventHandlers/onbeforeunload
pref("dom.disable_beforeunload",true);

pref("dom.webnotifications.enabled",false);

// Fully disable webgl
pref("webgl.disabled",true);

// Disable JavaScript
// pref("javascript.enabled",false);

// Disable downloadable fonts
// pref("gfx.downloadable_fonts.enabled",false);

// No autoplay (Video and Audio)
// Value obtained by experiment
pref("media.autoplay.default", 5);

// Don't leak timing information
// See https://wiki.mozilla.org/Security/Reviews/Firefox/NavigationTimingAPI
pref("dom.enable_performance",false);

// Disable WebRTC (leaks ip adress)
// See https://wiki.mozilla.org/Media/WebRTC/Privacy
pref("media.peerconnection.enabled",false);

// Enable tracking protection
// See https://wiki.mozilla.org/Security/Tracking_protection
pref("privacy.trackingprotection.enabled",true);
pref("privacy.trackingprotection.fingerprinting.enabled",true);
pref("privacy.trackingprotection.cryptomining.enabled",true);
pref("privacy.trackingprotection.socialtracking.enabled",true);

// Don't remember form content / autofill it
pref("browser.formfill.enable",false);

// Don't remember passwords
pref("signon.rememberSignons",false);

// Disable the captive portal service by default
pref("network.captive-portal-service.enabled",false);

// Do not trust the "Trusted Recursive Resolver"
pref("network.trr.mode",5);

// Safe browsing feature
// See https://support.mozilla.org/en-US/kb/how-does-phishing-and-malware-protection-work/

// Disable completely
pref("browser.safebrowsing.enabled",false);
// Just to be sure.. cf https://bugzilla.mozilla.org/show_bug.cgi?id=1025965
pref("browser.safebrowsing.malware.enabled",false);
pref("browser.safebrowsing.phishing.enabled",false);
pref("browser.safebrowsing.downloads.enabled",false);


// Referer header
// See https://wiki.mozilla.org/Security/Referrer
//

// Send referer header for clicked links only
pref("network.http.sendRefererHeader",1);

// Send a referrer only when the base domains are the same
pref("network.http.referer.XOriginPolicy",1);

// Disable browsing history
pref("places.history.enabled",false);

// Disable Telemetry
// See https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/internals/preferences.html

// Quote: "This is the data submission master kill switch.
// If disabled, no policy is shown or upload takes place, ever."
lockPref("datareporting.policy.dataSubmissionEnabled",false);

// lockPref is essential here (doesn't work otherwise)
// in other places its just for good measure.
lockPref("toolkit.telemetry.enabled",false);

lockPref("toolkit.telemetry.unified",false);
lockPref("toolkit.telemetry.archive.enabled",false);
lockPref("datareporting.healthreport.uploadEnabled",false);

// Just to be sure..
lockPref("toolkit.telemetry.server","");

// In case somethings slips through, we might notice
// with this set (dumps to stdout).
pref("toolkit.telemetry.log.dump",true);


// Removal of unneeded stuff
//

pref("browser.tabs.animate",false);
pref("toolkit.cosmeticAnimations.enabled",false);
pref("browser.blink_allowed",false);
pref("extensions.webcompat-reporter.enabled",false);
pref("browser.uitour.enabled",false);

// Caches
//

// Disable the disk cache
pref("browser.cache.disk.enable",false);
pref("browser.cache.disk.capacity",0);

// Disable website offline storage
pref("browser.cache.offline.enable",false);
pref("browser.cache.offline.capacity",0);

// Disable dom storage and indexedDB for websites
pref("dom.storage.enabled",false);
pref("dom.indexedDB.enabled",false);

// Forget closed tabs every 5min = 300secs
pref("browser.sessionstore.cleanup.forget_closed_after",300000);


// Cookies
// See https://developer.mozilla.org/en-US/docs/Mozilla/Cookies_Preferences
//

// Allow cookies from originating server only
pref("network.cookie.cookieBehavior",1);

// Prompt user for lifetime for each cookie
pref("network.cookie.lifetimePolicy",2);

// Disable DNS cache
pref("network.dnsCacheEntries",0);
pref("network.dnsCacheExpiration",0);
pref("network.dnsCacheExpirationGracePeriod",0);


// Disable speculative / undesired connections
// See https://support.mozilla.org/en-US/kb/how-stop-firefox-making-automatic-connections
//

// No dns prefetching 
pref("network.dns.disablePrefetch",true);

// Don't prefetch pages silently
pref("network.prefetch-next",false);
pref("network.predictor.enabled",false);

// Don't preopen connections
pref("network.http.speculative-parallel-limit",0);

// Prevent downloading the OpenH264 codec provided by Cisco
pref("media.gmp-gmpopenh264.enabled",false);

// "Send Video To Device" feature, should be disabled by default, but still..
pref("browser.casting.enabled",false);

// Disable https://wiki.mozilla.org/Advocacy/heartbeat
pref("browser.selfsupport.url","");

// Disable geolocation service
pref("geo.enabled",false);

// Don't load geolocation for default search engine
pref("browser.search.geoip.url","");

// Don't update about:home snippets
pref("browser.aboutHomeSnippets.updateUrl","");

// Don't send metadata about addons
pref("extensions.getAddons.cache.enabled",false);

// No connections before I press enter please
pref("browser.urlbar.speculativeConnect.enabled",false);

// Disable search suggestions (and associated background connections)
pref("browser.search.suggest.enabled");

// Don't check for search engine updates
pref("browser.search.update",false);


// The "newtab page"
//

pref("browser.newtabpage.enabled",false);
pref("browser.newtabpage.activity-stream.feeds.snippets", false);
pref("browser.newtabpage.activity-stream.feeds.section.highlights", false);
pref("browser.newtabpage.activity-stream.showSearch", false);
pref("browser.newtabpage.activity-stream.showTopSites", false);
pref("browser.newtabpage.activity-stream.aboutHome.enabled", false);
pref("browser.newtabpage.activity-stream.prerender",false);

// Seems to be the only way to disable "newtab page" connections
// fully even if the newtab page is set to different (local) page.
lockPref("browser.newtabpage.directory.source","");
lockPref("browser.newtabpage.directory.ping","");
lockPref("browser.newtabpage.activity-stream.asrouter.providers.snippets","");
lockPref("browser.newtabpage.activity-stream.telemetry.ping.endpoint","");
lockPref("browser.newtabpage.activity-stream.default.sites","");

// Don't create screenshots of visited pages for the "newtab page"
// See https://developer.mozilla.org/en-US/docs/Mozilla/Preferences/Preference_reference/browser.pagethumbnails.capturing_disabled
pref("browser.pagethumbnails.capturing_disabled",true);


// Performance related
//

// Needed for content.notify.*
pref("content.notify.ontimer",true);
// Render at most every 0.5 secs
pref("content.notify.interval",500000);
// Backoff at most 3 times, after that
// wait for page to load fully.
pref("content.notify.backoffcount",3);
// Delay for 150ms before painting (default is 250)
pref("nglayout.initialpaint.delay",150);

// Custom stuff
//


pref("browser.tabs.insertAfterCurrent",true);

// Sane focusing behaviour
pref("browser.autofocus",false);

// Don't search from the adress bar
pref("keyword.enabled",false);

// Collect suggestions from open tabs only
pref("browser.urlbar.suggest.bookmark",false);
pref("browser.urlbar.suggest.history",false);
pref("browser.urlbar.suggest.openpage",true);

// Preserve tabs / windows across restarts
pref("browser.startup.page", 3);

// Use blank homepage
pref("browser.startup.homepage","about:blank");

// Open links in new tab by default
pref("browser.search.openintab",true);

// Ask where to download
pref("browser.download.useDownloadDir", false);

// Sane search settings
pref("browser.search.hiddenOneOffs","Amazon.com,Bing,eBay,Google,Twitter");
lockPref("browser.search.searchEnginesURL","");

// Disable smooth scrolling
pref("general.smoothScroll",false);

// Go back on hitting backspace
pref("browser.backspace_action",0);

// Prevent sites from hijacking keyboard shortcuts
pref("permissions.default.shortcuts",2)

// Block autorefresh
pref("accessibility.blockautorefresh",true);

// Remove warning on about:config
pref("browser.aboutConfig.showWarning", false);

// Don't show special page when the browser was updated
pref("browser.startup.homepage_override.mstone","ignore");

// Don't make default search engines list geolocation dependent
// pref("browser.search.geoSpecificDefaults",false);
