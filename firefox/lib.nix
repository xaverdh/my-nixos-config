{ pkgs }:
rec {

  mkAddon = id: dir: pkgs.runCommand "firefox-extension" {} ''
    cd ${dir}
    mkdir -p $out
    ${pkgs.zip}/bin/zip -r $out/extension.zip *
    ${pkgs.utillinux}/bin/rename extension.zip "${id}.xpi" $out/extension.zip
  '';

  mkExtXpi = id: dir: "file://${mkAddon id dir}/${id}.xpi";

}
