{
  DisableAppUpdate = true;
  DisableFirefoxAccounts = true;
  DisableFirefoxStudies = true;
  DisableFirefoxScreenshots = true;
  DisableFormHistory = true;
  DisableMasterPasswordCreation = true;
  DisablePocket = true;
  DisableSystemAddonUpdate = true;
  DisableTelemetry = true;
  NetworkPrediction = false;
  CaptivePortal = false;
  SearchBar = "separate";
  DontCheckDefaultBrowser = true;
  OverrideFirstRunPage = "";
  OverridePostUpdatePage = "";
  NoDefaultBookmarks = true;
  DisplayBookmarksToolbar = false;
  DisplayMenuBar = "default-off";

  DNSOverHTTPS.Enabled = false;

  SanitizeOnShutdown = {
    Cache = true;
    Cookies = true;
    Downloads = true;
    FormData = true;
    History = false;
    Sessions = true;
    SiteSettings = true;
    OfflineApps = true;
  };

  SearchSuggestEnabled = false;

  SearchEngines = {
    Add = [
      {
        Name = "DuckDuckGoHTML";
        URLTemplate = "https://html.duckduckgo.com/html/?q={searchTerms}";
        Method = "GET";
        IconURL = "https://duckduckgo.com/assets/logo_icon128.v101.png";
        Alias = "dd";
        Description = "Pure html Version of DuckDuckGo";
      }
      {
        Name = "Hackage";
        URLTemplate = "https://hackage.haskell.org/packages/search?terms={searchTerms}";
        Method = "GET";
        IconURL = "https://hackage.haskell.org/static/favicon.png";
        Alias = "hackage";
        Description = "Search Hackage for packages";
      }
      {
        Name = "Hoogle";
        URLTemplate = "https://hoogle.haskell.org/?hoogle={searchTerms}";
        Method = "GET";
        IconURL = "https://hoogle.haskell.org/favicon.png";
        Alias = "hoogle";
        Description = "Search Hoogle for Haskell expressions";
      }
      {
        Name = "dict.cc";
        URLTemplate = "https://www.dict.cc/?s={searchTerms}";
        Method = "GET";
        IconURL = "https://www4.dict.cc/img/favicons/favicon4.png";
        Alias = "dict";
        Description = "Dictionary";
      }
      {
        Name = "Arch Wiki";
        URLTemplate = "https://wiki.archlinux.org/index.php?search={searchTerms}";
        Method = "GET";
        IconURL = "https://wiki.archlinux.org/favicon.ico";
        Alias = "aw";
        Description = "Search the Arch Wiki";
      }
      {
        Name = "NixOS Discourse";
        URLTemplate = "https://discourse.nixos.org/search?q={searchTerms}";
        Method = "GET";
        IconURL = "https://nixos.org/favicon.png";
        Alias = "nd";
        Description = "Search on NixOS Discourse";
      }
      {
        Name = "Nixpkgs Github Search";
        URLTemplate = "https://github.com/NixOS/nixpkgs/search?q={searchTerms}";
        Method = "GET";
        IconURL = "https://github.com/fluidicon.png";
        Alias = "ng";
        Description = "Search Nixpkgs on Github";
      }

    ];
    Default = "DuckDuckGoHTML";
    PreventInstalls = true;
    Remove = [ "Google" "Bing" "eBay" "Twitter" "Amazon.com" ];
  };

  UserMessaging = {
    WhatsNew = false;
    ExtensionRecommendations = false;
    FeatureRecommendations = false;
    UrlbarInterventions = false;
  };
}
