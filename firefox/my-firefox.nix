{ config
, pkgs
, lib
, my-lib
, passOptions ? {}
, withScripting ? false
, darkTheme ? false
}:

let
  home = config.custom.pantarhei.home;

  extensions = let
    decentraleyes = {
      id = "jid1-BoFifL9Vbdl2zQ@jetpack";
      url =
        "https://addons.mozilla.org/firefox/downloads/latest/decentraleyes/latest.xpi";
    };
    noscript = {
      id = "{73a6fe31-595d-460b-a920-fcc0f8843232}";
      url =
        "https://addons.mozilla.org/firefox/downloads/latest/noscript/latest.xpi";
    };
    disable-js = {
      id = "{41f9e51d-35e4-4b29-af66-422ff81c8b41}";
      url = "https://addons.mozilla.org/firefox/downloads/latest/disable-javascript/latest.xpi";
    };
    vim-vixen = {
      id = "vim-vixen@i-beam.org";
      url = "https://addons.mozilla.org/firefox/downloads/latest/vim-vixen/latest.xpi";
    };
  in pkgs.lib.optionals withScripting [ decentraleyes noscript ] ++ [ vim-vixen ];

  extPolicies = {
    ExtensionSettings = lib.fold (a: b: a // b) {} (
      builtins.map (ext: lib.optionalAttrs (ext ? "id" && ext ? "url") {
        "${lib.getAttr "id" ext}" = {
          installation_mode = "force_installed";
          install_url = lib.getAttr "url" ext;
        };
      } )
      extensions
    );
  };

  firefoxNixpkgs = pkgs.firefox.override (
    {
      extraPolicies = import ./policies.nix // extPolicies;
  #    nixExtensions = let
  #      decentraleyes = fetchFirefoxAddon {
  #        name = "decentraleyes";
  #        url = "https://addons.mozilla.org/firefox/downloads/latest/decentraleyes/latest.xpi";
  #        sha256 = "sha256-JVQGkWhFjME1Hl037pld4ETlFOxyNpgp/wvL8x1Ybd8=";
  #      };
  #      noscript = fetchFirefoxAddon {
  #        name = "noscript";
  #        url = "https://addons.mozilla.org/firefox/downloads/latest/noscript/latest.xpi";
  #        sha256 = "sha256-Pj4AAm22plzV7rvkosIU/X70N8j00X+lkchfYeq50eU=";
  #      };
  #      disable-js = fetchFirefoxAddon {
  #        name = "disable-js";
  #        url = "https://addons.mozilla.org/firefox/downloads/latest/disable-javascript/latest.xpi";
  #        sha256 = "sha256-01eNyjjeVK4/jx44GzcfYxdr7c11PlPNf1rwIJST3Hw=";
  #      };
  #      vim-vixen = fetchFirefoxAddon {
  #        name = "vim-vixen";
  #        url = "https://addons.mozilla.org/firefox/downloads/latest/vim-vixen/latest.xpi";
  #        sha256 = "sha256-T4fWDbBJs5pm/RMv/qNg4XBj54jt+a9SPikd0HX8340=";
  #      };
      extraPrefs = ''
        pref("browser.download.dir","${home}/Downloads");
        pref("browser.download.folderList",2);
        ${pkgs.lib.optionalString (!withScripting) ''
          pref("javascript.enabled",false);
          pref("gfx.downloadable_fonts.enabled",false);
        ''}
        ${pkgs.lib.optionalString darkTheme ''
          pref("lightweightThemes.selectedThemeID", "firefox-compact-dark@mozilla.org");
          pref("devtools.theme","dark");
        ''}
        ${builtins.readFile ./prefs.js}
      '';
    }
  );

  maybePass = n: a: lib.optionalAttrs (passOptions ? ${a})
    { ${n} = lib.getAttr a passOptions; };

in my-lib.exeWrapper ({
  wrapped = { package = firefoxNixpkgs; bin = "firefox"; };
  command = lib.optionalString (passOptions ? profileLocation)
    "mkdir -p ${passOptions.profileLocation}";
  args = lib.cli.toGNUCommandLineShell {} (
      maybePass "profile" "profileLocation"
      // maybePass "class" "wmClass"
      );
} // maybePass "wrapperName" "exeName")
