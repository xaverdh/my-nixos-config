{ secrets, pkgs, lib, ... }:
pkgs.lib.mapAttrs
  (n: p: pkgs.my-lib.exeWrapper (p { inherit pkgs lib secrets; }))
  {
    alacritty = import ./wrappers/alacritty.nix;
    neomutt-runbox = attrs: import ./wrappers/neomutt.nix
      (attrs // { account = "runbox"; });
    neomutt-gmx = attrs: import ./wrappers/neomutt.nix
      (attrs // { account = "gmx"; });
    offlineimap = import ./wrappers/offlineimap.nix;
    abook = import ./wrappers/abook.nix;
    khal = import ./wrappers/khal.nix;
    vdirsyncer = import ./wrappers/vdirsyncer.nix;
    tig = import ./wrappers/tig.nix;
    wget = import ./wrappers/wget.nix;
    bluetoothctl = import ./wrappers/bluetoothctl.nix;
  }
