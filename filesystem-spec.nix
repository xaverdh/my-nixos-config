{
  persist = prefix: [
    "d ${prefix}/shadow 0755 0 0"
    "d ${prefix}/var 0755 0 0"
    "d ${prefix}/var/lib 0755 0 0"
    "d ${prefix}/var/lib/nixos 0755 0 0"
    "d ${prefix}/var/lib/nix-store-key 0755 0 0"
    "d ${prefix}/var/log 0755 0 0"
    "d ${prefix}/var/tmp 1777 0 0"
    "d ${prefix}/var/cache 0755 0 0"
  ];

  data = prefix: [
    "v ${prefix}/store 0755 0 0"
    "v ${prefix}/nix-var 0755 0 0"
  ];
}
