{ pkgs, lib }:
let
   inherit (lib)
     optionalString
     optional
     optionals
     concatStringsSep
     ;
in rec {

  # systemd units
  systemd = {
    mkStartupScript = { desc ? "", script }: {
      description = desc;
      script = script;
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = true;
      };
      wantedBy = [ "multi-user.target" ];
    };

    mkResumeScript = { desc ? "", script }: {
      description = desc;
      after = [ "suspend.target" ];
      script = script;
      serviceConfig = {
        Type = "oneshot";
      };
      wantedBy = [ "suspend.target" ];
    };
  };

  udev.rules = {
    formatPci = { vendor, device, action }: ''
      ACTION=="add", SUBSYSTEM=="pci", ATTR{vendor}=="${vendor}", ATTR{device}=="${device}", ${action}
    '';
    formatUsb = { vendor, device, action }: ''
      ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="${vendor}", ATTR{idProduct}=="${device}", ${action}
    '';
  };

  mount =
    let genericBind = lazy:
      { src, options ? null, owner ? null, group ? null, mode ? null, nofail ? true }: {
        device = src;
        fsType = "none";
        noCheck = true;
        options = [ "bind" ]
        ++ lib.optional nofail "nofail"
        ++ lib.optionals lazy [ "noauto" "x-systemd.automount" ]
        ++ lib.optional (owner != null) "X-mount.owner=${owner}"
        ++ lib.optional (group != null) "X-mount.group=${group}"
        ++ lib.optional (mode != null) "X-mount.mode=${mode}"
        ++ lib.optionals (options != null) options;
      };
    in {

    # bind mount with sane options
    bind = genericBind true;

    # non lazy bind mount with sane options
    bindStrict = genericBind false;

    # overlay mount with sane options
    overlay =
      { lowerdirs
      , upperdir ? null
      , workdir ? null
      , requiredMounts ? []
      , options ? null
      }: {
        device = "overlay";
        fsType = "overlay";
        options = [
          "nofail"
          "noauto"
          "x-systemd.automount"
          "lowerdir=${concatStringsSep ":" lowerdirs}"
        ]
        ++ map (s: "x-systemd.requires-mounts-for=${s}") requiredMounts
        ++ optionals (upperdir != null && workdir != null)
          [ "upperdir=${upperdir}" "workdir=${workdir}" ]
        ++ optionals (options != null) options;
        noCheck = true;
      };

    btrfs = { device, subvol ? null, nofail ? true, extraOptions ? [] }: {
      inherit device;
      fsType = "btrfs";
      options = optional (subvol != null) "subvol=${subvol}"
      ++ optional nofail "nofail" ++ [ "noatime" ] ++ extraOptions;
    };

  };

  exeWrapper =
    { wrapped
    , wrapperName ? wrapped.bin
    , args ? null
    , command ? null
    , vars ? []
    , defaultVars ? []
    }:
    let
      inherit (lib) escapeShellArg;

      setVars = concatStringsSep " "
        (
          map
            (v: "--set ${escapeShellArg v.var} ${escapeShellArg v.val}")
            vars
        );
      setDefaultVars = concatStringsSep " "
        (
          map
            (v: "--set-default ${escapeShellArg v.var} ${escapeShellArg v.val}")
            defaultVars
        );
      setFlags = optionalString (args != null)
        "--add-flags ${escapeShellArg args}";
      setCommand = optionalString (command != null)
        "--run ${escapeShellArg command}";
    in
      pkgs.stdenv.mkDerivation {
        name = "${wrapped.bin}-wrapper-${wrapperName}";
        buildCommand = ''
          makeWrapper ${wrapped.package}/bin/${wrapped.bin} $out/bin/${wrapperName} ${setFlags} ${setCommand} ${setVars} ${setDefaultVars}
        '';
        buildInputs = [ pkgs.makeWrapper ];
        preferLocalBuild = true;
        description = "${wrapped.bin} wrapper";
        meta.mainProgram = wrapped.bin;
      };

  withClang = p: p.override {
    stdenv = pkgs.llvmPackages_latest.stdenv;
  };

  linuxWithClang = p: pkgs.recurseIntoAttrs
    (pkgs.linuxPackagesFor (withClang p));

  fetchGitL = { url ? null, lpath ? null, rev, hash ? null, sha256 ? null }:
    if lpath != null then builtins.fetchGit { url = lpath; }
    else pkgs.fetchgit ({ inherit url rev; } // (if hash != null then { inherit hash; } else { inherit sha256; }));

  # haskell specific functions

  haskell = hp: rec {

    mkPackage =
      { name
      , src
      , doProfiling ? false
      , doHaddock ? false
      , cabal2nixOptions ? ""
      }:
      let
         hlib = pkgs.haskell.lib;
      in args: lib.pipe
          ( hp.callCabal2nixWithOptions name src cabal2nixOptions args )
          (
            optionals (!doProfiling)
              [
                hlib.disableLibraryProfiling
                hlib.disableExecutableProfiling
              ]
            ++ optional (!doHaddock) hlib.dontHaddock
          );

  };

  # dependencies

  dependsOn =
    let have = deps: ! lib.any builtins.isNull deps;
    in deps: optionalString (have deps);

  # generic functions

  bindAttrs = f: attrs: lib.fold (a: b: a // b) {} (lib.mapAttrsToList f attrs);

  autocall = f: values: f (builtins.intersectAttrs values (builtins.functionArgs f));

  at = x: f: f x;

  fromMaybe = empty: val: if isNull val then empty else val;

  maybe = f: val: mbVal: if mbVal == null then val else f mbVal;

}
