# My nixos configuration
{ config, pkgs, lib, ... }:
{
  imports =
    [
      # Include custom modules.
      ./custom-modules.nix
      # Include my system packages.
      ./system-packages.nix
      # Include packages for pantarhei.
      ./pantarhei-packages.nix
      # Include home directory setup for pantarhei.
      ./pantarhei-home.nix
      # Include my fish configuration.
      ./fish/config.nix
    ];

  services.fwupd.enable = true;

  services.pipewire = {
    enable = true;
    pulse.enable = true;
  };

  i18n.defaultLocale = "en_GB.UTF-8";

  console = {
    keyMap = "de";
    font = "ter-v32b";
    earlySetup = true;
    packages = with pkgs; [ terminus_font ];
  };

  # time zone is state, use symlink (cf. timedatectl) to manage it
  time.timeZone = null;

  boot.postBootCommands = ''
    tzlink=$(readlink /persist/localtime)
    test -f "$tzlink" && ln -s "$tzlink" /etc/localtime || echo "could not set timezone from /persist/localtime"
  '';

  services.dbus.implementation = "broker";

  services.userborn = {
    enable = true;
    passwordFilesLocation = "/var/lib/nixos";
  };

  environment.etc."machine-id".source = "/persist/machine-id";

  boot.initrd.systemd.enable = true;

  location.provider = lib.mkDefault "geoclue2";

  fonts = {
    enableDefaultPackages = true;
    packages = with pkgs; [
      noto-fonts
      noto-fonts-extra
      noto-fonts-emoji
    ];
    fontconfig.defaultFonts = {
      monospace = [ "Noto Sans Mono" ];
      serif = [ "Noto Serif" ];
      sansSerif = [ "Noto Sans" ];
      emoji = [ "Noto Color Emoji" ];
    };
  };

  services.kmscon = {
    enable = true;
    autologinUser = "pantarhei";
    hwRender = true;
    extraConfig = ''
      xkb-layout=de
      #xkb-variant=neo
      #font-name=
      font-size=20
    '';
  };

  services.getty.autologinUser = "pantarhei";

  environment.variables = {
    EDITOR = "vim";
    BROWSER = "links";
    LESSHISTFILE = "-";
  };

  programs.bash.completion.enable = true;

  programs.light.enable = true;

  programs.adb.enable = true;

  security.pki.certificateFiles = [ "${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt" ];

  services.usbguard = {
    rules = ''
      allow with-interface equals { 08:*:* }
    '';
    IPCAllowedGroups = [ "wheel" ];
  };

  services.udisks2.enable = true;

  programs.ssh.extraConfig = ''
    ControlMaster auto
    ControlPersist 5m
    ControlPath ~/.ssh/sockets/socket-%r@%h:%p
  '';

  programs.gnupg.agent = {
    enable = true;
    pinentryPackage = pkgs.pinentry-curses;
  };

  programs.iotop.enable = true;

  users.mutableUsers = false;
  users.users.pantarhei = {
    isNormalUser = true;
    createHome = true;
    home = config.custom.pantarhei.home;
    extraGroups = [ "wheel" "video" "adbusers" ];
    shell = pkgs.fish;
    packages = config.custom.pantarhei.pkgs;
    hashedPasswordFile = "/persist/shadow/pantarhei";
  };

  users.users.root = {
    hashedPasswordFile = "/persist/shadow/root";
    hashedPassword = null;
  };

  security.sudo.extraConfig = ''
    Defaults lecture = never
    Defaults passwd_timeout = 0
  '';

  programs.command-not-found.enable = false;

  programs.direnv.enable = true;

  # disable "classical desktop" related stuff
  xdg = lib.listToAttrs (
    lib.forEach
      [ "autostart" "menus" "mime" "sounds" ]
      (cat: lib.nameValuePair "${cat}" { enable = lib.mkForce false; })
  );

  gtk.iconCache.enable = false;

  documentation.man = {
    mandoc.enable = true;
    man-db.enable = false;
  };

  nixpkgs.overlays = [
    (import ./pkgs/list.nix { inherit config; })
  ];

  nix = {
    channel.enable = false;
    settings = {
      trusted-users = [ "root" "@wheel" ];
      keep-outputs = true;
      keep-derivations = true;
      keep-env-derivations = true;
      auto-optimise-store = true;
      secret-key-files = "/var/lib/nix-store-key/cache-secret";
      experimental-features = "nix-command flakes auto-allocate-uids cgroups";
      auto-allocate-uids = true;
      use-cgroups = true;
    };
    daemonCPUSchedPolicy = "idle";
    daemonIOSchedClass = "idle";
  };

}
