{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs, ... }: {

    nixosConfigurations.tux = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        (import ./configuration.nix)
        (import ./per-box/tux/hardware-configuration.nix)
        (import ./per-box/tux)
        { nix.registry.nixpkgs.flake = nixpkgs; }
      ];
    };

    nixosConfigurations.sony-box = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        (import ./configuration.nix)
        (import ./per-box/sony-box/hardware-configuration.nix)
        (import ./per-box/sony-box)
        { nix.registry.nixpkgs.flake = nixpkgs; }
      ];
    };

    nixosConfigurations.generic = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        (import ./configuration.nix)
        (import ./per-box/generic/hardware-configuration.nix)
        (import ./per-box/generic)
        { nix.registry.nixpkgs.flake = nixpkgs; }
      ];
    };

    nixosConfigurations.pegasus = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        (import ./configuration.nix)
        (import ./per-box/pegasus/hardware-configuration.nix)
        (import ./per-box/pegasus)
        { nix.registry.nixpkgs.flake = nixpkgs; }
      ];
    };

    fileSystemSpec = (import ./filesystem-spec.nix);

  };
}

