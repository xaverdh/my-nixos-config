# My fish configuration:
{ config, lib, pkgs, ... }:
let
  inherit (pkgs) eza xclip;
in {
    programs.fish.enable = true;

    programs.fish.promptInit = ''
      function fish_prompt
        set laststatus $status
        set_color -o blue
        printf "%s" (get_prompt_state)' '

        if test $laststatus -eq 0
          set_color -o normal
          printf '<'$laststatus'>'
        else
          set_color -o red
          printf '<'$laststatus'>'
        end

        echo

        if set -q IN_NIX_SHELL
          set_color -o green
          printf "%s" "IN_NIX_SHELL"' '
        end
        set git_branch (git branch --show-current 2>/dev/null)
        if test -n "$git_branch"
          set_color -o normal
          printf "%s" "on branch: "
          set_color -o green
          echo "$git_branch"
        end

        set_color -o normal
        printf ' >>= '
      end
    '';

    programs.fish.interactiveShellInit = ''
          set_color -o blue
          set sep '   '
          set logo \
        $sep'                     ' \
            '         /\          ' \
            '        /%s\         ' \
            '       /\  /\        ' \
            '      /%s\/%s\       ' \
            '     /\  /\  /\      ' \
            '    /%s\/  \/%s\     ' \
            '   /\  /\\\%s/\  /\    ' \
            '  /  \/%s\/%s\/%s\   ' \
            ' /\\\%s/\  /\  /\  /\  ' \
            '/%s\/%s\/%s\/  \/%s\ ' \
            '\  /\  /\  /\  /\  / ' \
            '\n'

          function gen_eyes
            for i in (seq 1 14)
              if test (random) -le 21844
                echo '..'
              else
                echo '  '
              end
            end
          end

          printf (string join $sep'\n'$sep $logo) (gen_eyes)
    
          set_color normal
      '';

    programs.fish.shellAliases = {
      ls = "${lib.getExe eza}";
      tree = "${lib.getExe eza} -T";
      xclip = "${lib.getExe xclip} -selection clipboard";
    };

    environment.systemPackages = pkgs.callPackage ./packages.nix { inherit config; };
  }
