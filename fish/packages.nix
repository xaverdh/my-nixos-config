{ 
  lib,
  config,
  wrappers,
  stdenv,
  nix,
  git,
  coreutils,
  mupdf,
  qrencode,
  feh,
  libnotify,
  inotify-tools,
  pandoc,
  nix-du,
  graphviz,
  gnugrep,
  bat,
  ponymix,
  ripgrep,
  lineage-update-verifier,
  nettools
}:
let
  inherit (lib) getExe;

  mkFishPackage = { name, ... }@args:
   let fileName = "${name}.fish";
   in stdenv.mkDerivation ({
      src = ./functions.d/${fileName};
      dontUnpack = true;
      buildPhase = ''
        substituteAll "$src" "${fileName}"
      '';
      installPhase = ''
        mkdir -p $out/share/fish/vendor_functions.d
        cp "${fileName}" $out/share/fish/vendor_functions.d/
      '';
    } // args);
  lineageos-pubkey = builtins.toFile "lineageos-pubkey" (builtins.readFile ../config/lineageos_pubkey); 
  persist = config.custom.pantarhei.persist;
in map mkFishPackage [
  { name = "get_prompt_state";
    whoami = lib.getExe' coreutils "whoami";
    date = lib.getExe' coreutils "date";
    hostname = lib.getExe' nettools "hostname"; }
  { name = "mupdf"; inherit mupdf; }
  { name = "tex_env"; nix = getExe nix; }
  { name = "tex_watch"; inherit libnotify; inotify_tools = inotify-tools; }
  { name = "abook2vcard"; abook = getExe wrappers.abook; }
  { name = "abook2qr"; qrencode = getExe qrencode; feh = getExe feh; }
  { name = "qr"; qrencode = getExe qrencode; feh = getExe feh; }
  { name = "readme"; pandoc = getExe pandoc; }
  # { name = "nixos_disk_usage"; inherit graphviz; feh = getExe feh; nix_du = getExe nix-du; }
  { name = "pathof"; inherit coreutils; }
  { name = "run"; nix = getExe nix; }
  { name = "nix_homepage"; nix = getExe nix; }
  { name = "blue_connect"; inherit libnotify persist;
    ponymix = getExe ponymix;
    ripgrep = getExe ripgrep; }
  { name = "lineage_verify";
    lineage_update_verifier = lineage-update-verifier;
    lineageos_pubkey = lineageos-pubkey;
  }
  { name = "checkout_pr"; git = getExe git; }
]
