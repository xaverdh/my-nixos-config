function tex_env
  @nix@ shell nixpkgs#texlive.combined.scheme-full $argv
end
