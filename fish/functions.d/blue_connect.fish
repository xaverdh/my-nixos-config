function blue_connect -a mode
  function get_con_status
    bluetoothctl -- info "$mac" \
        | @ripgrep@ --replace "" "\s*Connected:\s*"
    or echo "no"
  end
  bluetoothctl -- power on
  if test -f @persist@/.bluetooth-autoconnect
    read mac desc < @persist@/.bluetooth-autoconnect
    bluetoothctl -- connect "$mac"
    set connected (get_con_status)
    @libnotify@/bin/notify-send "connected: $connected"
    if test "$connected" = "yes"
      switch "$mode"
        case "headset"
          # switch to headset mode
          @ponymix@ set-profile headset_head_unit
          set dev (string replace -a ':' '_' -- "$mac")".headset_head_unit"
          @ponymix@ -d "bluez_sink.$dev" --sink set-default
          @ponymix@ -d "bluez_source.$dev" --source set-default
        case "music"
          if set -q argv[2]
            set player "-p" $argv[2]
          else
            set player
          end
          # make player stop on disconnect
          watch-bluez-player-disconnect \
            && @playerctl@ $player stop
      end
    end
  end
end
