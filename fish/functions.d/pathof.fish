function pathof -a cmd -w type
  @coreutils@/bin/readlink -f (type -P "$cmd" 2> /dev/null)
end
