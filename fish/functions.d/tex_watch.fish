function tex_watch
  if test (count $argv) -le 1
    echo "usage: texwatch FILE_TO_COMPILE FILES_TO_WATCH.."
  else
    while true
      @inotify_tools@/bin/inotifywait \
        -e modify -e close_write $argv[2..-1]
      if lualatex --interaction=nonstopmode $argv[1]
        killall -s SIGHUP mupdf-x11
      else
        @libnotify@/bin/notify-send "failed to compile"
      end
    end
  end
end
