function run
  if set -q argv[1]
    @nix@ run nixpkgs#"$argv[1]" -- $argv[2..-1]
  end
end
