function nixos_disk_usage
  @nix_du@ -s 500MB | @graphviz@/bin/tred | @graphviz@/bin/dot -Tpng | @feh@ -
end
