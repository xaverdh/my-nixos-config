function readme
  if test -z "$argv"
    set readme_path "README.md"
  else
    set readme_path "$argv"
  end
  @pandoc@ --standalone --from gfm --to man -o - "$readme_path" | mandoc
end
