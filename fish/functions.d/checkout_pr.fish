function checkout_pr -a pr_no branch_name
  if test (count $argv) -lt 2
    echo "usage: checkout_pr pr_no branch_name"
  else
    echo "fetching pr $pr_no with local branch name $branch_name"

    @git@ fetch origin "refs/pull/$pr_no/head:$branch_name"
    and git checkout "$branch_name"
    and echo "checked out $branch_name"
  end
end
