function get_prompt_state
  printf '%s\n' (@whoami@)'@'(@hostname@)
  prompt_pwd
  set_color -o grey
  @date@ "+%H:%M on %a %d %b %Y"
  set_color normal
end
