function nix_homepage
  @nix@ eval --raw nixpkgs#"$argv" --apply 'p: p.meta.homepage'
  echo
end
