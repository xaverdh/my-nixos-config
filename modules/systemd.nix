{ config, lib, pkgs, ... }:
let
  cfg = config.custom;
in
{

  options.custom = {};

  config = {

    systemd.services.display-manager.serviceConfig.Restart = lib.mkForce "no";

    services.journald.extraConfig = ''
      SystemMaxUse=50M
    '';

    systemd.extraConfig = ''
      DefaultTimeoutStartSec=30s
      DefaultTimeoutStopSec=30s
    '';

    services.logind.extraConfig = ''
      KillUserProcesses=yes
    '';

  };

}
