{ config, lib, pkgs, ... }:
let
  inherit (lib) types;
  inherit (lib.options) mkOption;
  cfg = config.custom;
in
{

  options.custom = {

    serveNix = mkOption {
      type = types.bool;
      default = false;
      description = ''
        Serve nix store via http
      '';
    };

  };

  config = {

    # Serve nix store via http (port 5000)
    services.nix-serve = {
      enable = true;
      package = pkgs.nix-serve-ng;
      secretKeyFile = "/var/lib/nix-serve/cache-secret";
    };
    systemd.services.nix-serve.wantedBy = lib.mkIf (!cfg.serveNix) (lib.mkForce []);
    networking.firewall.allowedTCPPorts = lib.optional cfg.serveNix 5000;
    nix.settings.trusted-public-keys = [
      "blumerang-1:oM28laH7uWz1iF06+gpDLXlnZ1vlQUBdtSRnsO/gEFU="
    ];

  };

}
