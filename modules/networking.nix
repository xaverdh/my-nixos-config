{ config, lib, pkgs, ... }:
let
  inherit (lib.options) mkEnableOption mkOption;
  cfg = config.custom.networking;
in
{

  options.custom.networking = {
    enable = mkEnableOption "iwd & networkd based networking" // { default = true; };
  };

  config = lib.mkIf cfg.enable {
    networking = {
      wireless.iwd = {
        enable = true;
        settings = {
          General = {
            AddressRandomization = "network";
            EnableNetworkConfiguration = true;
          };
        };
      };
      useDHCP = false;
      useNetworkd = true;
      dhcpcd.enable = false;
    };

    systemd.network.networks."en-dhcp" = {
      name = "en*";
      DHCP = "yes";
    };

    systemd.network.networks."eth-dhcp" = {
      name = "eth*";
      DHCP = "yes";
    };

    systemd.network.wait-online = {
      enable = false;
      anyInterface = true;
    };
    
    services.resolved.dnssec = "false";
  };
}  
