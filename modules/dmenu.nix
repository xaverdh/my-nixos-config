{ config, lib, pkgs, ... }:
let
  inherit (lib) types optionalString;
  inherit (lib.options) mkEnableOption mkOption mkIf;
  cfg = config.custom.programs.dmenu;
  inherit (pkgs) my-lib;
in
{

  options.custom.programs.dmenu = {

    enable = mkEnableOption "configure dmenu";

    font = {
      name = mkOption {
        type = types.str;
        default = null;
        description = ''
          Set the font to be used by dmenu.
        '';
      };
      size = mkOption {
        type = types.nullOr types.int;
        default = null;
        description = ''
          Set the font size.
        '';
      };
    };
    extraCliOptions = mkOption {
      type = types.str;
      default = "";
      description = ''
        This gets appended verbatim to the command line of dmenu.
      '';
    };

  };

  config = let
    sizeStr = optionalString (cfg.font.size != null) ":size=${toString cfg.font.size}";
    fontStr = optionalString (cfg.font.name != null) "-fn '${cfg.font.name}${sizeStr}'";
    dmenu_run = my-lib.exeWrapper {
      wrapped = { package = pkgs.dmenu; bin = "dmenu_run"; };
      args = fontStr;
    };
    dmenu = my-lib.exeWrapper {
      wrapped = { package = pkgs.dmenu; bin = "dmenu"; };
      args = fontStr;
    };
    dmenu_path = my-lib.exeWrapper {
      wrapped = { package = pkgs.dmenu; bin = "dmenu_path"; };
    };
  in
    lib.mkIf cfg.enable {
      environment.systemPackages = [ dmenu dmenu_run dmenu_path ];
    };

}
