{ config, lib, pkgs, ... }:
let
  inherit (lib)
    types
    optionalString
    concatStringsSep
    mapAttrsToList
    ;
  inherit (lib.options) mkOption;
  cfg = config.custom.kernel;
  inherit (pkgs) my-lib;
in
{

  options.custom.kernel = {

    package = mkOption {
      type = types.attrs;
      default = pkgs.linuxPackages_latest;
      description = ''
        Kernel package to use
      '';
    };

    ramCompression = mkOption {
      type = types.enum [ "zram" "zswap" "none" ];
      default = "zram";
      description = ''
        Whether to use zram / zswap for memory compression.
      '';
    };

    cpuIsIntel = mkOption {
      type = types.bool;
      default = false;
      description = ''
        The cpu is known to be a recent intel cpu.
      '';
    };

    pciPowersave = {
      enable = mkOption {
        type = types.bool;
        default = true;
        description = ''
          Enable pci runtime power management.
        '';
      };
      exempt = mkOption {
        type = types.attrsOf (
          types.submodule {
            options = {
              vendor = mkOption {
                example = "8086";
                type = types.str;
              };
              device = mkOption {
                example = "9d2f";
                type = types.str;
              };
            };
          }
        );
        default = {};
        description = ''
          Devices to exempt from pci powersaving.
        '';
      };
    };

    usbPowersave = {
      enable = mkOption {
        type = types.bool;
        default = true;
        description = ''
          Enable usb runtime power management.
        '';
      };
      exempt = mkOption {
        type = types.attrsOf (
          types.submodule {
            options = {
              vendor = mkOption {
                example = "05c6";
                type = types.str;
              };
              device = mkOption {
                example = "9205";
                type = types.str;
              };
            };
          }
        );
        default = {};
        description = ''
          Devices to exempt from usb powersaving.
        '';
      };
    };


  };

  config = {

    environment.systemPackages = [ cfg.package.cpupower ];

    boot = {
      # Linux kernel
      kernelPackages = cfg.package;


      kernelModules = [
        "cpufreq_userspace"
      ];

      kernelParams =
        lib.optionals (cfg.ramCompression == "zswap") [
          "zswap.enabled=1"
          "zswap.max_pool_percent=50"
        ]
        ++ lib.optionals cfg.cpuIsIntel [ "intel_pstate=passive" ]
        ++ [
          "cpufreq.default_governor=schedutil"
          "ia32_emulation=off"
          "bdev_allow_write_mounted=n"
        ];

      kernel.sysctl = { "vm.dirty_writeback_centisecs" = 1500; };

      initrd.compressor = "zstd";
      initrd.compressorArgs = [ "--ultra" "-22" ];

    };

    zramSwap.enable = cfg.ramCompression == "zram";

    systemd.services.set-perf-bias = lib.mkIf cfg.cpuIsIntel {
      description = "CPU Perf Bias Setup";
      after = [ "systemd-modules-load.service" "suspend.target" ];
      wantedBy = [ "multi-user.target" "suspend.target" ];
      path = [ cfg.package.cpupower pkgs.kmod ];
      serviceConfig = {
        Type = "oneshot";
        ExecStart = "${lib.getExe' cfg.package.cpupower "cpupower"} set -b 15";
      };
    };

    hardware.cpu.intel.updateMicrocode = true;

    services.udev.extraRules = 
      let
        usbExemptRules = concatStringsSep "\n"
          ( mapAttrsToList
            ( name: attrs: my-lib.udev.rules.formatUsb {
              action = "GOTO=\"power_usb_rules_end\"";
              inherit (attrs) vendor device;
            } )
            cfg.usbPowersave.exempt );
        pciExemptRules = concatStringsSep "\n"
          ( mapAttrsToList
            ( name: attrs: my-lib.udev.rules.formatPci {
                action = "GOTO=\"power_pci_rules_end\"";
                inherit (attrs) vendor device;
              } )
            cfg.pciPowersave.exempt );
      in optionalString cfg.usbPowersave.enable ''
       # usb autosuspend:
       ${usbExemptRules}
       ACTION=="add", SUBSYSTEM=="usb", TEST=="power/control", ATTR{power/autosuspend_delay_ms}="10000"
       ACTION=="add", SUBSYSTEM=="usb", TEST=="power/control", ATTR{power/control}="auto"
       LABEL="power_usb_rules_end"
      ''
      + optionalString cfg.pciPowersave.enable ''
        # pci runtime power management:
        ${pciExemptRules}
        ACTION=="add", SUBSYSTEM=="pci", ATTR{power/control}="auto"
        LABEL="power_pci_rules_end"
      '' + ''
        # Use none IO scheduler for nvme drives
        ACTION=="add|change", KERNEL=="nvme[0-9]n[0-9]", ATTR{queue/scheduler}="none"
      '';

    powerManagement.scsiLinkPolicy = "med_power_with_dipm";

    powerManagement.cpuFreqGovernor = lib.mkDefault null;

  };

}
