{ config, lib, pkgs, ... }:
let
  inherit (lib) types;
  inherit (lib.options) mkOption mkEnableOption literalExample;
  inherit (pkgs) my-lib;
  cfg = config.custom.mounts;
  home = config.custom.pantarhei.home;

  btrfs = args: my-lib.mount.btrfs ({
    device = cfg.btrfsDevice;
    extraOptions = [ "compress=zstd" ];
  } // args);

  bindPersist = path: my-lib.mount.bindStrict { src = "/persist${path}"; nofail = false; };

  persistSpec = (import ../filesystem-spec.nix).persist "/persist";

in
{
  options.custom.mounts = {

    enable = mkEnableOption "custom mounts" // { default = true; };

    btrfsDevice = mkOption {
      type = types.str;
      description = ''
        The btrfs device path
      '';
    };

    bindExtraSubvolumes = mkOption {
      type = types.attrsOf (types.submoduleWith {
        modules = [ {
          options.subvol = mkOption {
            type = types.str;
            description = ''
              Path of the subvolume (relative to the toplevel subvolume)
            '';
          };
        } ];
      });
      default = {};
      description = ''
        Extra subvolumes to mount in pantarhei home directory
      '';
      example = literalExample ''
        { "Mail".subvol = "mail"; }
      '';
    };

    persistMount = mkOption {
      type = types.attrs;
      description = ''
        Mount spec for the persistence partition
      '';
    };

    bootMount = mkOption {
      type = types.attrs;
      description = ''
        Mount spec for the boot partition
      '';
    };

    extraPersistentPaths = mkOption {
      type = types.listOf types.str;
      default = [ "/var/lib" "/var/log" "/var/tmp" "/var/cache" ];
      description = ''
        Extra paths to bind mount to the persistence partition
      '';
    };

    rootTmpfsSize = mkOption {
      type = types.str;
      default = "90%";
      description = ''
        Size of the root tmpfs
      '';
    };

  };

  config.fileSystems =
    {
         "/" = {
           device = "none";
           fsType = "tmpfs";
           options = [
             "defaults" "mode=755"
             "size=${cfg.rootTmpfsSize}"
           ];
         };
        "/nix/store" = btrfs { subvol = "store"; nofail = false; };
        "/nix/var" = btrfs { subvol = "nix-var"; nofail = false; };
        "/mnt/btrfs" = btrfs {};
        "/persist" = cfg.persistMount // { neededForBoot = true; };
        "/boot" = cfg.bootMount;
      }
      // ( let f = path: spec: {
                     "${home}/${path}" = btrfs { inherit (spec) subvol; };
                   };
            in my-lib.bindAttrs f cfg.bindExtraSubvolumes )
      // ( let f = path: lib.nameValuePair
                          path (bindPersist "${path}");
            in lib.listToAttrs (builtins.map f cfg.extraPersistentPaths) );

  config.boot.supportedFilesystems = {
    btrfs = true;
    zfs = lib.mkForce false;
  };

  config.systemd.tmpfiles.rules = persistSpec;

}
