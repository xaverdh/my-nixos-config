# My X configuration.
{ config, pkgs, lib, ... }:
let
  haskellPackages = pkgs.haskellPackages;

  xmonadConfig = builtins.readFile (pkgs.my-xmonad-src + "/xmonad.hs");

  xmonadExtraPackages = hpkgs: with hpkgs; [
    extra data-default
    (pkgs.my-xmonad-lib haskellPackages)
  ];

  cfg = config.custom.xserver;

in
{

  options.custom.xserver = {
    displayBlanking = lib.mkEnableOption "automatic display blanking";
    displayTimeoutMinutes = lib.mkOption {
      type = lib.types.int;
      default = 3;
    };
  };

  config = {
      services.dunst = let q = s: ''"${s}"''; in {
      enable = true;
      globalConfig = {
        min_icon_size = "48";
        font = "Monospace 20";
        markup = "full";
        width = "(0, 600)";
        height = "800";
        offset = "20x20";
        shrink = "true";
        icon_position = "right";
        frame_width = "1";
        frame_color = q "#aaaaaa";
        separator_color = "auto";
        format = ''"<b>%s</b>\n%b"'';
        follow = "none";
      };
      urgencyConfig = {
        normal = {
          background = q "#0C5B6A";
          #background = q "#222222";
          foreground = q "#888888";
          frame_color = q "#ffffff";
          timeout = "10";
        };
        low = {
          background = q "#0C5B6A";
          foreground = q "#ffffff";
          frame_color = q "#ffffff";
          timeout = "10";
        };
        critical = {
          background = q "#900000";
          foreground = q "#ffffff";
          frame_color = q "#ffffff";
          timeout = "0";
        };
      };
    };
  
    services.displayManager = {
      autoLogin = {
        enable = true;
        user = "pantarhei";
      };
    };

    services.libinput = {
      enable = true;
      touchpad = {
        middleEmulation = false;
        disableWhileTyping = true;
        sendEventsMode = "disabled-on-external-mouse";
      };
    };

    services.xserver = {
      enable = true;
      xkb.layout = "de";
      xkb.options = "caps:escape";

      displayManager.lightdm = {
        enable = true;
        greeter.enable = false;
      };

      windowManager.xmonad = {
        enable = true;
        enableContribAndExtras = true;
        extraPackages = xmonadExtraPackages;
        inherit haskellPackages;
        config = xmonadConfig;
        ghcArgs = [ "-O2" ];
      };
  
      modules = [ pkgs.xf86_input_wacom ];
      enableCtrlAltBackspace = true;
  
      serverFlagsSection =
        let timeout = if cfg.displayBlanking then builtins.toString cfg.displayTimeoutMinutes else "0";
        in ''
        Option "BlankTime" "0"
        Option "StandbyTime" "0"
        Option "SuspendTime" "0"
        Option "OffTime" "${timeout}"
      '';
    };
  
    services.picom = {
      enable = true;
      backend = "egl";
    };
  
    services.redshift = {
      enable = true;
      temperature = {
        day = 5500;
        night = 2500;
      };
      brightness = {
        day = "1.0";
        night = "0.8";
      };
      #extraOptions = [ "-m drm:crtc=0" ];
    };
    systemd.user.services.redshift.wantedBy = lib.mkForce [ "default.target" ];
    systemd.user.services.redshift.partOf = lib.mkForce [ "default.target" ];
  
  
    environment.systemPackages = with pkgs; [
      feh # for setting background image
      xorg.xset
      xorg.xrandr
      # quality of live:
      oneko
      xclip
      xdragon
      xqr
      tigervnc
    ];
  
    custom.programs.dmenu = {
      enable = true;
      font = pkgs.lib.mkDefault {
        name = "Noto Sans Mono";
        size = 16;
      };
    };
  };
}
