{ config, lib, pkgs, ... }:
let
  inherit (lib) types;
  inherit (lib.options) mkOption;
  cfg = config.custom.terminalEmulator;
  inherit (pkgs) wrappers;
  terminal-wrapper = pkgs.writeShellScriptBin "terminal" ''
    exec ${cfg.package}/bin/${cfg.binName}
  '';
in
{

  options.custom = {

    terminalEmulator = {
      binName = mkOption {
        type = types.str;
        default = "alacritty";
        description = ''
          Name of the terminal emulator binary
        '';
      };
      package = mkOption {
        type = types.package;
        default = wrappers.alacritty;
        description = ''
          The terminal emulator package to use
        '';
      };
    };

  };

  config = {
    environment.systemPackages = [ cfg.package terminal-wrapper ];
  };

}
