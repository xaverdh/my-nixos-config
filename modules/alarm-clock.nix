{ config, lib, pkgs, ... }:
let
  inherit (lib.options) mkEnableOption mkOption;
  cfg = config.custom.alarmClock;
in
{

  options.custom.alarmClock = {
    enable = mkEnableOption "alarm clock" // { default = true; };
  };

  config = lib.mkIf cfg.enable {
    systemd.user.services.alarm = {
      serviceConfig.ExecStart = "${lib.getExe pkgs.mpv} -vid=no /var/lib/alarm/";
    };
  };

}
