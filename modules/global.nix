{ config, lib, pkgs, ... }:
let
  inherit (lib) types;
  inherit (lib.options) mkOption;
  cfg = config.custom;
in
{

  options.custom = {

    boxName = mkOption {
      type = types.str;
      description = ''
        The name of the box we are building for.
      '';
    };

    minimal = mkOption {
      type = types.bool;
      default = false;
      description = ''
        This is a minimal install.
      '';
    };

    pantarhei = {

      home = mkOption {
        type = types.str;
        default = "/home/pantarhei";
        description = ''
          The home directory of user pantarhei
        '';
      };

      persist = mkOption {
        type = types.str;
        default = "/persist/home/pantarhei";
        description = ''
          Where to store persistent data (for pantarhei)
        '';
      };

      secrets = mkOption {
        type = types.str;
        default = "/home/pantarhei/.secrets";
        description = ''
          Where to store secret data (for pantarhei)
        '';
      };

      pkgs = mkOption {
        type = types.listOf types.package;
        default = [];
        description = ''
          The packages of user pantarhei
        '';
      };
    };

  };

}
