# My system packages
{ config, pkgs, ... }:
let
  inherit (pkgs) wrappers vimPlugins;

  my-vim = pkgs.vim_configurable.customize {
    name = "vim";
    vimrcConfig.customRC = ''
      if !isdirectory($HOME . "/.local/share/vim/backup")
        call mkdir($HOME . "/.local/share/vim/backup", "p", 0700)
      endif
      if !isdirectory($HOME . "/.local/share/vim/swp")
        call mkdir($HOME . "/.local/share/vim/swp", "p", 0700)
      endif
      set backupdir=~/.local/share/vim/backup
      set directory=~/.local/share/vim/swp//
      set backupcopy=yes
      set viminfo=""
      syntax on
      set bg=dark
      set history=1000
      set bs=indent,eol,start
      set ruler
      set expandtab softtabstop=2 shiftwidth=2
      set autoindent
      set nomodeline
    '';
    vimrcConfig.packages = {
      editorconfig.start = [
        pkgs.vimPlugins.editorconfig-vim
      ];
    };
  };
  my-neovim = pkgs.neovim.override {
    configure = {
      customRC = ''
        if !isdirectory($HOME . "/.local/share/nvim/backup")
          call mkdir($HOME . "/.local/share/nvim/backup", "p", 0700)
        endif
        set backupcopy=yes
        set backupdir=~/.local/share/nvim/backup
        set background=dark
        filetype plugin indent on
        set hlsearch
        set whichwrap=b,s,<,>,[,]
        set expandtab softtabstop=2 shiftwidth=2
        set autoindent
        set history=100
        set ruler
        set showcmd
        set nomodeline

        luafile ${pkgs.writeText "neovim-config-lua" (builtins.readFile ./config/nvim.lua)}
      '';
      packages = {
        editorconfig.start = [
          vimPlugins.editorconfig-vim
        ];
        nvim-lspconfig.start = [
          vimPlugins.nvim-lspconfig
        ];
        nvim-treesitter.start = [
          vimPlugins.nvim-treesitter.withAllGrammars
        ];
      };
    };
  };

in
{
  environment.systemPackages = with pkgs; [
    nix-prefetch-git
    nix-index
    man-pages

    cryptsetup
    strace
    nmap
    tcpdump
    iptables
    nftables
    sshfs
    dnsutils
    wrappers.wget
    curl
    my-vim
    my-neovim
    file
    zip
    unzip
    git
    gitAndTools.lab
    gitAndTools.hub
    efibootmgr
    efivar
    btrfs-progs
    exfat
    ntfsprogs
    ntfs3g
    gptfdisk
    hdparm
    htop
    powertop
    ncdu
    lm_sensors
    acpi
    psmisc
    moreutils
    pciutils
    usbutils
    android-file-transfer
    ffmpeg

    eza
    ripgrep
    bingrep
    fd
    bat
    jq
    runiq
    ponymix
    pdfgrep
    wavemon

    gnupg
    gpgme
    pinentry
    pandoc
    qrencode
    zbar
    pwgen
    mu

    irssi
    mosh
    screen
    links2
    feh
    scrot
    # jfbview
    mupdf
    guvcview

    libnotify
    redshift # for manual mode

    cabal-install
    cabal2nix
    haskellPackages.hserv

    (rebuild-nixos.override {
      boxName = config.custom.boxName;
    })

    unnix

  ] ++ lib.optionals (!config.custom.minimal) [
    gcc rustup haskellPackages.ghc
  ];

}
