# My box specific configuration.
{ config, pkgs, lib, ... }:

let
  home = config.custom.pantarhei.home;
  inherit (pkgs) my-lib;
in
{

  custom.boxName = "tux";

  custom.mounts = {

    persistMount = {
      device = "/dev/disk/by-partuuid/4f0c7657-2e12-7b45-a017-993a31d86e44";
      fsType = "ext4";
      options = [ "lazytime" "noatime" ];
    };

    bootMount = {
      device = "/dev/disk/by-partuuid/7e73e410-569c-7147-932b-a3274eca3403";
      fsType = "vfat";
      options = [ "lazytime" "noatime" ];
    };

    btrfsDevice = "/dev/disk/by-partuuid/dec108fc-9a8e-ab47-9239-37ad6d283df5";
    bindExtraSubvolumes = {
      "config".subvol = "config";
      "git".subvol = "git";
      "data".subvol = "data";
      "Mail".subvol = "mail";
      ".secrets".subvol = "secrets";
    };

    rootTmpfsSize = "4G";
  };

  custom.pantarhei.pkgs = with pkgs; [
    signal-desktop
    tdesktop
  ];

  custom.xserver = {
    displayBlanking = true;
  };

  services.radicale = {
    enable = true;
    package = pkgs.radicale;
    settings = {
      server = {
        hosts = [ "0.0.0.0:5232" "[::]:5232" ];
      };
      auth = {
        type = "htpasswd";
        htpasswd_filename = "/var/lib/radicale/users";
        htpasswd_encryption = "plain";
      };
    };
  };

  systemd.tmpfiles.rules = [
    "d /var/lib/radicale 0755 radicale radicale"
  ];

  systemd.user.services.flashmq =
    let flashmq-config = pkgs.writeText "flashmq.conf" ''
        expire_retained_messages_after_seconds 86400
        allow_anonymous true
        listen {
          protocol mqtt
          port 1883
        }
      '';
    in {
    enable = true;
    serviceConfig = {
      ExecStart = "${lib.getExe' pkgs.flashmq "flashmq"} --config-file ${flashmq-config}";
      ExecReload = "${lib.getExe' pkgs.util-linux "kill"} -HUP $MAINPID";
      Restart = "on-failure";
      RestartSec = "5s";
    };
    unitConfig = {
      Description = "FlashMQ MQTT server";
    };
    wantedBy = [ "default.target" ];
  };

  systemd.user.services.twilight =
    let python = pkgs.python3.withPackages (ps: with ps; [ astral paho-mqtt inotify ]);
        twilight = pkgs.writers.makeScriptWriter {
          interpreter = python.interpreter;
        } "twilight" ./twilight.py;
    in {
    enable = true;
    requires = [ "flashmq.service" ];
    serviceConfig = {
      ExecStart = "${twilight}";
      Restart = "on-failure";
      RestartSec = "5s";
    };
    unitConfig = {
      Description = "Adjust light according to the position of the sun";
    };
    wantedBy = [ "default.target" ];
  };

  systemd.user.services.htwatch =
    let python = pkgs.python3.withPackages (ps: with ps; [ paho-mqtt ]);
        htwatch = pkgs.writers.makeScriptWriter {
          interpreter = python.interpreter;
        } "htwatch" ./htwatch.py;
    in {
      enable = true;
      serviceConfig = {
        ExecStart = "${htwatch}";
        Restart = "on-failure";
        RestartSec = "5s";
      };
      unitConfig.Description = "capture temperature and humidity data";
      wantedBy = [ "default.target" ];
    };

  system.autoUpgrade = {
    enable = true;
    flake = "git+file:///var/lib/my-nixos-config#${config.custom.boxName}";
    flags = [ "--update-input" "nixpkgs" ];
    operation = "boot";
    allowReboot = true;
    randomizedDelaySec = "90min";
  };

  custom.kernel = {
    cpuIsIntel = true;
  };

  custom.serveNix = true;

  networking.timeServers = [
    "ntp1.uni-augsburg.de"
    "ntp2.uni-augsburg.de"
    "ntp3.uni-augsburg.de"
    "ntp4.uni-augsburg.de"
    "0.nixos.pool.ntp.org"
    "1.nixos.pool.ntp.org"
    "2.nixos.pool.ntp.org"
    "3.nixos.pool.ntp.org"
  ];

  services.openssh = {
    enable = true;
    settings = {
      PasswordAuthentication = false;
    };
    hostKeys = [
      {
        bits = 4096;
        path = "/var/lib/ssh/ssh_host_rsa_key";
        type = "rsa";
      }
      {
        path = "/var/lib/ssh/ssh_host_ed25519_key";
        type = "ed25519";
      }
    ];
  };

  networking.firewall.enable = false;

  services.logind.lidSwitch = "ignore";

  boot = {

    # Use the systemd-boot EFI boot loader.
    loader = {
      systemd-boot.enable = true;
      systemd-boot.editor = true;
      systemd-boot.configurationLimit = 20;
      efi.canTouchEfiVariables = true;
    };

    kernelParams = [ "i915.enable_psr=1" ];

    initrd.kernelModules = [ "i915" ];

    extraModprobeConfig = ''
      options snd_hda_intel power_save=1
      options iwlwifi power_save=1
    '';

  };

  hardware.bluetooth.enable = false;
  hardware.bluetooth.powerOnBoot = false;

  hardware.graphics.enable = pkgs.lib.mkForce false;
  services.picom.enable = pkgs.lib.mkForce false;

  custom.terminalEmulator = {
    binName = "termite";
    package = pkgs.termite;
  };

  location.provider = "geoclue2";

  networking.hostName = "blumerang";

  services.usbguard.enable = false;

  system.stateVersion = "19.09";
}
