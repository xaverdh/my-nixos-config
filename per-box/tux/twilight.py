#!/usr/bin/env nix-shell
#!nix-shell -I nixpkgs=flake:nixpkgs -i python -p "with import (builtins.getFlake "flake:nixpkgs") {}; python3.withPackages (ps: with ps; [ astral paho-mqtt inotify ])"

import astral, astral.sun
import time, datetime, math, itertools
import paho.mqtt.publish as publish
import os, inotify.adapters
import multiprocessing as mp


class SunEvent:
  def __init__(self,b):
    self.brightness = b

class ConfigEvent:
  def __init__(self, light, name, value):
    self.light = light
    self.name = name
    self.value = value


def clip(lower,upper,value):
  return max(lower,min(upper,value))

def brightness(observer):
  elevation = astral.sun.elevation(observer=observer)
  zenith = astral.sun.zenith(observer=observer)
  print(elevation,zenith)
  if elevation > 0:
    b = math.cos(zenith * math.pi / 180) * 2 * 100
  else:
    b = 2 * elevation
  return clip(25,90,40 + b)

def checkpoints(observer):
  while True:
    today = datetime.date.today()
    tomorrow = datetime.date.fromordinal(datetime.date.toordinal(today) + 1)

    timestamps = []
    for el in itertools.chain(range(-100,100),range(10 * (180-10),10 * (180+10))):
      timestamps.append( astral.sun.time_at_elevation(observer=observer, date=today, elevation=el/10).timestamp() )
      timestamps.append( astral.sun.time_at_elevation(observer=observer, date=tomorrow, elevation=el/10).timestamp() )
    timestamps.sort()
    
    for t in timestamps:
      yield t

def wait_till(t):
  while True:
    now = time.time()
    if t < now:
      break
    else:
      time.sleep(clip(0,60,t - now))

def strip_newlines(strings):
  return [ s.removesuffix("\n") for s in strings ]

def read_value(path, cast):
  try:
    with open(path) as file:
      [ v ] = strip_newlines(file.readlines())
      return cast(v)
  except (FileNotFoundError,ValueError):
    return None

def watch_config(q):
  i = inotify.adapters.Inotify()
  for light in os.scandir("/var/lib/twilight/lights"):
    if light.is_dir():
      for f in filter( lambda e: e.is_file(), os.scandir(light.path) ):
        q.put(ConfigEvent( light.name, f.name, read_value(f.path,float) ))
        i.add_watch(f.path)
  for (_, type_names, path, _) in i.event_gen(yield_nones=False):
    i.remove_watch(path)
    light = os.path.basename(os.path.dirname(path))
    file = os.path.basename(path)
    q.put(ConfigEvent(light, file, read_value(path,float) ))
    i.add_watch(path)

def watch_sun(observer,q):
  startup = time.time()
  pts = checkpoints(observer)
  while True:
    q.put(SunEvent(brightness(observer)))
    now = time.time()
    pts = itertools.dropwhile(lambda t: t < now, pts)
    wait_till(next(pts))

class ShelliesMQTT:
  def __init__(self, hostname, port = 1883):
    self.hostname = hostname
    self.port = port
  def publish(self, topic, payload):
    publish.single(
        topic,
        payload,
        hostname=self.hostname,
        port=self.port,
        retain=True
      )
  def set(self, light, values):
    payload = ', '.join( (f"{k}: {v}" for k,v in values.items()) )
    self.publish(
        f"shellies/{light}/light/0/set",
        f"{{ {payload} }}"
      )
  def publish_values(self, light, config, sun_brightness):
    attrs = { "transition": 5000 }
    b = config.get("brightness") or sun_brightness
    t = config.get("temperature")
    if b:
      attrs["brightness"] = round(b)
    if t:
      attrs["temp"] = round(t)
    self.set(light, attrs)


def __main__():
  shellies = ShelliesMQTT(hostname="localhost")

  timezone = datetime.datetime.now(datetime.timezone.utc).astimezone()

  with open("/var/lib/twilight/location") as loc:
    lat, long = strip_newlines(loc.readlines())

  observer = astral.LocationInfo(
      name='current',
      region='current',
      timezone=timezone.tzname(),
      latitude=float(lat),
      longitude=float(long)
    ).observer
  
  q = mp.Queue()
  mp.Process(target=watch_config,args=[q]).start()
  mp.Process(target=watch_sun,args=[observer,q]).start()

  config = dict()
  sun_brightness = None

  while True:
    e = q.get()
    print(e)
    match e:
      case ConfigEvent():
        config.update({ e.light: { e.name : e.value} })
        shellies.publish_values(e.light, config[e.light], sun_brightness)
      case SunEvent():
        sun_brightness = e.brightness
        for light in config.keys():
          shellies.publish_values(light, config[light], sun_brightness)

__main__()
