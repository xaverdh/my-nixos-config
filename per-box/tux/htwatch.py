#!/usr/bin/env nix-shell
#!nix-shell -i python3 -p "with import (builtins.getFlake \"flake:nixpkgs\") {}; python3.withPackages (ps: with ps; [ astral paho-mqtt inotify ])"

import paho.mqtt.subscribe as subscribe
import time
import datetime
from pathlib import Path

def on_message(client, userdata, msg):
  t = datetime.datetime.now()
  value = float(msg.payload)
  sensor = msg.topic.removeprefix("shellies/shellyht-CC1F97/sensor/")
  logfile = f"{Path.home()}/data/sensors/{sensor}-log.csv"
  with open(logfile,mode='a') as log:
    print(f"{value} at time {t}")
    log.write(f"{round(t.timestamp())},{value}\n")

sensor_path = lambda v: f"shellies/shellyht-CC1F97/sensor/{v}"

subscribe.callback(on_message,[ sensor_path(v) for v in ("temperature","humidity") ])

