# My box specific configuration.
{ config, pkgs, ... }:
{

  custom.minimal = true;

  custom.boxName = "generic";

  custom.mounts = {

    persistMount = {
      device = "/dev/disk/by-partlabel/generic-persist";
      fsType = "ext4";
      options = [ "lazytime" "noatime" ];
    };

    bootMount = {
      device = "/dev/disk/by-partlabel/generic-esp";
      fsType = "vfat";
      options = [ "lazytime" "noatime" ];
    };

    btrfsDevice = "/dev/disk/by-partlabel/generic-data";
    bindExtraSubvolumes = {
      "config".subvol = "config";
      "git".subvol = "git";
      "data".subvol = "data";
      ".secrets".subvol = "secrets";
    };
  };

  boot = {
    # Use the systemd-boot EFI boot loader.
    loader = {
      systemd-boot.enable = true;
      systemd-boot.editor = true;
    };
  };

  location.provider = "geoclue2";

  networking.hostName = "gaugeintegration";

}
