# My hardware specific configuration.
{ config, pkgs, lib, ... }:

let
  funcs = config.custom.functions;
  home = config.custom.pantarhei.home;

  hfish-collection = import ../hfish/hfish-collection.nix {
    inherit config pkgs;
  };

  texbuilder-dev = with pkgs;
    (funcs.haskell haskellPackages).mkPackage {
      name = "texbuilder";
      lpath = "${config.custom.pantarhei.home}/git/texbuilder";
      local = true;
    } {};

in
{

  custom.boxName = "sony-box";


  custom.mounts = {

    persistMount = {
      device = "/dev/disk/by-label/nixos";
      fsType = "ext4";
      options = [ "lazytime" "noatime" ];
    };

    bootMount = {
      device = "/dev/disk/by-label/EFI";
      fsType = "vfat";
      options = [ "lazytime" "noatime" ];
    };

    btrfsDevice = "/dev/disk/by-label/sony-box-data";

    bindExtraSubvolumes = {
      "config".subvol = "config";
      "git".subvol = "git";
      "data".subvol = "data";
      "Mail".subvol = "mail";
      ".secrets".subvol = "secrets";
    };

  };

  custom.pantarhei.pkgs = with pkgs; [
    signal-desktop
  ];

  custom.kernel.cpuIsIntel = true;

  networking.timeServers = [
    "ntp1.uni-augsburg.de"
    "ntp2.uni-augsburg.de"
    "ntp3.uni-augsburg.de"
    "ntp4.uni-augsburg.de"
    "0.nixos.pool.ntp.org"
    "1.nixos.pool.ntp.org"
    "2.nixos.pool.ntp.org"
    "3.nixos.pool.ntp.org"
  ];

  boot = {

    # Use the systemd-boot EFI boot loader.
    loader = {
      systemd-boot.enable = true;
      systemd-boot.editor = true;
      efi.canTouchEfiVariables = true;
    };

    blacklistedKernelModules = [
      "pn544"
      "pn544_mei"
      "mei_phy"
      "hci"
      "nfc"
      # ^ disable nfc related modules, not needed anyway
    ];
    kernelParams = [
      "video=HDMI-A-1:1440x900@60e"
      "video=eDP-1:d"
      "usbcore.autosuspend=-1"
      # ^ usb subsystem wakes system immediately after suspend
    ];

    initrd.kernelModules = [ "i915" ];

    extraModprobeConfig = ''
      options snd_hda_intel power_save=1
    '';

  };

  powerManagement.powerUpCommands = ''
    # Set fan driver to silent mode
    echo silent > /sys/devices/platform/sony-laptop/thermal_control
    # Set battery charging limit to 80%
    echo 80 > /sys/devices/platform/sony-laptop/battery_care_limiter
    # Remap the second shift key on my broken keyboard to function as arrow key
    ${lib.getBin pkgs.kbd}/bin/setkeycodes 36 106
  '';

  hardware.bluetooth.enable = false;

  networking.hostName = "ringswithoutrings";

  # Latex is added here as to not fill the
  # user env with unnecessary cruft.
  system.extraDependencies = [ pkgs.texlive.combined.scheme-full ];

  services.logind.lidSwitch = "ignore";

  system.autoUpgrade = {
    enable = false;
    allowReboot = false;
    dates = "12:00";
  };

  system.stateVersion = "19.09";

}
