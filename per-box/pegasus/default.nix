# My box specific configuration.
{ config, pkgs, lib, ... }:

let
  home = config.custom.pantarhei.home;
in
{

  custom.boxName = "pegasus";

  custom.mounts = {

    persistMount = {
      device = "/dev/disk/by-partlabel/pegasus-persist";
      fsType = "ext4";
      options = [ "lazytime" "noatime" ];
    };

    bootMount = {
      device = "/dev/disk/by-partlabel/pegasus-esp";
      fsType = "vfat";
      options = [ "lazytime" "noatime" ];
    };

    btrfsDevice = "/dev/disk/by-partlabel/pegasus-data";

    bindExtraSubvolumes = {
      "config".subvol = "config";
      "git".subvol = "git";
      "data".subvol = "data";
      "Mail".subvol = "mail";
      ".secrets".subvol = "secrets";
    };

    rootTmpfsSize = "28G";
  };

  boot.kernelParams = [ "hpet=disable" "tsc=unstable" "trace_clock=local" "resume=PARTLABEL=\"pegasus-persist\"" "resume_offset=227328" ];

  systemd.tmpfiles.rules = [ "w /sys/power/image_size - - - - 0" ];

  swapDevices = [
    { device = "/persist/swapfile"; }
  ];

  services.snapper.configs = {
    mail = {
      SUBVOLUME = "/mnt/btrfs/mail";
      ALLOW_USERS = [ "pantarhei" ];
      TIMELINE_CREATE = true;
      TIMELINE_CLEANUP = true;
    };
  };

  boot.extraModprobeConfig = ''
    options mac80211 beacon_loss_count=3000
    options iwlwifi swcrypto=0
  '';

  boot = {
    loader = {
      systemd-boot.enable = true;
      systemd-boot.editor = true;
    };
  };


  hardware.tuxedo-rs = {
    enable = true;
  };

  environment.etc."/tailord".source = "/var/lib/tailord/";


  powerManagement.resumeCommands = ''
    ${lib.getExe pkgs.tuxedo-silence-fan}
  '';

  services.logind.lidSwitch = "hibernate";

  custom.pantarhei.pkgs = [
    pkgs.signal-desktop
  ];

  hardware.bluetooth = {
    enable = true;
    powerOnBoot = false;
  };

  hardware.cpu.amd.updateMicrocode = true;

  boot.initrd.availableKernelModules = [ "amdgpu" ];

  location.provider = "geoclue2";

  networking.firewall.enable = false;

  networking.hostName = "pegasus";

  services.xserver.xkb.options = "compose:menu";

  system.stateVersion = "22.05";

}
