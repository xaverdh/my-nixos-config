{ config, lib, pkgs, modulesPath, ... }:

{

  hardware.enableRedistributableFirmware = lib.mkDefault true;

  boot.initrd.availableKernelModules = [ "nvme" "xhci_pci" "rtsx_pci_sdmmc" ];

  nixpkgs.hostPlatform = "x86_64-linux";

}
