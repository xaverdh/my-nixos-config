use tuxedo_ioctl::hal::IoInterface;
fn main() {
    let interface = IoInterface::new().unwrap();
    interface.device.set_fan_speed_percent(0,0).unwrap();
}
