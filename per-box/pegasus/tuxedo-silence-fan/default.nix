{ rustPlatform, fetchFromGitHub, ... }:
let
    tuxedo-rs = fetchFromGitHub {
      owner = "AaronErhardt";
      repo = "tuxedo-rs";
      rev = "f356f05d8fe40de3c5121c11a5a9b37047501440";
      hash = "sha256-RVauCQn8T6q+c7z0nYGtxdV/89IWAGSsqHL2xm0OTgI=";
    };
in rustPlatform.buildRustPackage {
  pname = "tuxedo-silence-fan";
  version = "0.1.0";
  src = ./.;
  cargoLock.lockFile = ./Cargo.lock;
  postPatch = ''
    cargo add --path ${tuxedo-rs}/tuxedo_ioctl
  '';
  meta.mainProgram = "tuxedo-silence-fan";
}
