{ pkgs, lib, ... }:
{
  wrapped = { package = pkgs.bluez; bin = "bluetoothctl"; };
  command = ''
    ${lib.getBin pkgs.bluez}/bin/bluetoothctl -- power on
  '';
}
