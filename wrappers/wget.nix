{ pkgs, ... }:
{
  wrapped = { package = pkgs.wget; bin = "wget"; };
  args = "--hsts-file=$HOME/.cache/wget-hsts";
}
