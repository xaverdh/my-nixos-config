{ lib, pkgs, secrets, account, batch ? false, ... }:
let
  settings = {
    colors = ''
      # Palette for use with the Linux console.  Black background.
      color hdrdefault brightblue black
      color quoted brightblue black
      color signature brightblue black
      color attachment red black
      color prompt brightmagenta black
      color message brightred black
      color error brightred black
      color indicator black red
      color status brightgreen brightblue
      color tree white black
      color normal white black
      color markers red black
      color search white black
      color tilde brightmagenta black
      color index brightblue black ~F
      color index red black "~N|~O"

      # color body brightwhite black '\*+[^*]+\*+'
      # color body brightwhite black '_+[^_]+_+'
    '';
    keys = ''
      bind index r imap-fetch-mail
      macro pager t "<pipe-message> pandoc --from html --to plain | less <enter>"
    '';
    maildir = ''
      set mbox_type=Maildir
      set folder="~/Mail/${account}/"
      set spoolfile="+INBOX"
    '';
  };

  muttrc = pkgs.writeText "muttrc-${account}" ''
    # -*-muttrc-*-

    # General
    ${lib.optionalString batch "set copy=no"}

    set editor = "vim +8"
    set use_threads = threads
    set sort = last-date
    set sort_aux = date
    set send_charset = "utf-8"
    set pager_stop = yes
    set pager_context = 3
    set markers = no
    set new_mail_command = "notify-send --icon=mail-unread 'New Mail' '%n new messages' &"

    ${settings.maildir}
    set record = +Gesendet
    macro attach s <save-entry><bol>$HOME/

    # Pipe decoded message instead of raw
    set pipe_decode = yes

    # Caching
    set header_cache = ~/.cache/mutt

    # Ssl
    unset ssl_starttls
    set ssl_force_tls = yes

    # Account specific stuff

    # sets account specific stuff (folder, smtp_url, smtp_pass, my_hdr and alternates)
    source $MUTT_SECRETS/account

    # Header
    set edit_headers = yes
    unset use_domain
    unset use_from
    # unset user_agent

    # Gpg
    set crypt_use_gpgme = yes
    set crypt_replyencrypt = yes
    set crypt_replysignencrypted = yes
    set pgp_default_key = "38F039E613D58FE34DC903F71B9462032AFD8B93"

    # Contacts
    set sort_alias = alias

    set query_command = "abook --mutt-query '%s'"
    macro index,pager a "<pipe-message>abook --add-email-quiet<return>" "Add this sender to Abook"
    bind editor <Tab> complete-query

    # Key bindings and colors
    ${settings.keys}
    ${settings.colors}
  '';
in
{
  wrapped = { package = pkgs.neomutt; bin = "neomutt"; };
  wrapperName = "${account}-mutt";
  args = pkgs.lib.cli.toGNUCommandLineShell {} {
    F = "${muttrc}";
  };
  defaultVars = [
    { var = "MUTT_CONFIG"; val = muttrc; }
    { var = "MUTT_SECRETS"; val = "${secrets}/mutt/${account}"; }
  ];
  command = ''
    if test ! -d ~/.cache/mutt; then
      test -f ~/.cache/mutt && rm ~/.cache/mutt
      test -f ~/.cache/mutt-lock && rm ~/.cache/mutt-lock
      mkdir ~/.cache/mutt
    fi
    cd ~/Mail/${account}
  '';
}
