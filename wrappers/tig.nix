{ pkgs, lib, ... }:
{
  wrapped = { package = pkgs.tig; bin = "tig"; };
  command = "${lib.getBin pkgs.coreutils}/bin/mkdir -p ~/.local/share/tig";
}
