{ pkgs, secrets, lib, ... }:
let
  secretDir = "${secrets}/mail";
  conf = builtins.toFile "offlineimap-config" (
    pkgs.lib.generators.toINI {} {
      general = {
        accounts = "gmx,runbox";
        maxsyncaccounts = "2";
      };
      general.pythonfile = builtins.toFile "offlineimap-config.py" ''
        def readutf8file(p):
          with open("${secretDir}/" + p) as fd:
            return fd.readline().strip()
        gethost = readutf8file
        getport = lambda p: int(readutf8file(p))
        getuser = lambda p: readutf8file(p).encode('UTF-8')
        getpass = lambda p: readutf8file(p).encode('UTF-8')
      '';
      "Account gmx" = {
        localrepository = "gmx-local";
        remoterepository = "gmx-remote";
        autorefresh = "5";
      };
      "Account runbox" = {
        localrepository = "runbox-local";
        remoterepository = "runbox-remote";
        autorefresh = "5";
      };

      "Repository gmx-local" = {
        type = "Maildir";
        localfolders = "~/Mail/gmx";
      };
      "Repository gmx-remote" = {
        type = "IMAP";
        remotehosteval = ''gethost("gmx/host")'';
        remoteporteval = ''getport("gmx/port")'';
        remoteusereval = ''getuser("gmx/username")'';
        remotepasseval = ''getpass("gmx/password")'';
        tls_level = "tls_secure";
        ssl_version = "tls1_2";
        sslcacertfile = "/etc/ssl/certs/ca-bundle.crt";
        createfolders = "False";
      };
      "Repository runbox-local" = {
        type = "Maildir";
        localfolders = "~/Mail/runbox";
      };
      "Repository runbox-remote" = {
        type = "IMAP";
        remotehosteval = ''gethost("runbox/host")'';
        remoteporteval = ''getport("runbox/port")'';
        remoteusereval = ''getuser("runbox/username")'';
        remotepasseval = ''getpass("runbox/password")'';
        tls_level = "tls_secure";
        ssl_version = "tls1_2";
        sslcacertfile = "/etc/ssl/certs/ca-bundle.crt";
        createfolders = "False";
      };
    }
  );
in
{
  wrapped = { package = pkgs.offlineimap; bin = "offlineimap"; };
  args = "-c ${conf}";
}
