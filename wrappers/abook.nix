{ pkgs, secrets, ... }:
let
  conf = builtins.toFile "abook-config" ''
    set index_format=" {name:20} {email:30} {phone:16|workphone|mobile} {anniversary:11} {address}"
  '';
in
{
  wrapped = { package = pkgs.abook; bin = "abook"; };
  args = pkgs.lib.cli.toGNUCommandLineShell {} {
    config = conf;
    datafile = "${secrets}/addressbook";
  };
}
