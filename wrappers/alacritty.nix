{ pkgs, ... }:
let
  conf = builtins.toFile "alacritty.toml"
    (builtins.readFile ../config/alacritty.toml);
in
{
  wrapped = { package = pkgs.alacritty; bin = "alacritty"; };
  args = pkgs.lib.cli.toGNUCommandLineShell {} {
    config-file = conf;
  };
}
