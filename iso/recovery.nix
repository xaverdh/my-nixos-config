{ config, lib, pkgs, modulesPath, ... }:
{
  imports = [
    "${modulesPath}/installer/cd-dvd/installation-cd-minimal-new-kernel-no-zfs.nix"

    # Provide an initial copy of the NixOS channel so that the user
    # doesn't need to run "nix-channel --update" first.
    "${modulesPath}/installer/cd-dvd/channel.nix"

    # Custom networking
    ../modules/networking.nix
  ];

  networking.wireless.enable = lib.mkForce false;

  environment.systemPackages = with pkgs; [
    zbar qrencode fish ripgrep fd git mkpasswd
    dislocker chntpw
    sedutil hdparm testdisk
  ];

  isoImage.squashfsCompression = "zstd -Xcompression-level 6";

}
